// We need to tell TypeScript that when we write "import styles from './styles.sass' we mean to load a module (to look for a './styles.sass.d.ts').
declare module "*.sass" {
    const content: { [className: string]: string };
    export = content;
 }