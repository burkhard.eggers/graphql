import {
  createContext,
  Context,
} from 'react';
import {context} from './base';
import { FullPerson, Person, Post } from '../../types';

interface State {
  persons: FullPerson[],
  selectedPerson?: FullPerson,
  loadPerson: boolean,
  loadPost: boolean,
  selectedDetailsOpen?: boolean
}

interface Mutators {
  setPersons: (persons: FullPerson[]) => void,
  deletePerson: (id: number) => void,
  insertPerson: (person: Person) => void, 
  setSelectedPerson: (selectedPerson: FullPerson) => void,
  setNameOfSelected: (name: string) => void,
  setAgeOfSelected: (age: string) => void,
  submitPerson: () => void,
  post: (post: Post) => void,
  setLoadPerson: (loadPerson: boolean) => void,
  setLoadPost: (loadPost: boolean) => void,
  setSelectedDetailsOpen: (selectedDetailsOpen: boolean) => void
}

export default context<State, Mutators>({
  initialState: {
    persons: [],
    loadPerson: false,
    loadPost: false
  },
  loggerName: 'persons-post',
  mutators: ({setState, state, setter}) => ({
    setPersons: setter<Person[]>('persons'),
    deletePerson: (id: number) => {
      setState({
        selectedPerson: state.selectedPerson?.id === id ? undefined : state.selectedPerson,
        persons: state.persons.filter((p) => p.id !== id) 
      });
    },
    insertPerson: (person: Person) => {
      setState({
        persons: [...state.persons, {...person, posts: []}] 
      })
    },
    setSelectedPerson: (selectedPerson: FullPerson) => {
      const persons = state.persons.map(p => p.id === selectedPerson.id ? selectedPerson : p);
      setState({
        selectedPerson: selectedPerson,
        persons
      })
    },
    setNameOfSelected: (name: string) => {
      const selectedPerson = {
        ...state.selectedPerson as FullPerson,
        name
      }
      setState({
        selectedPerson
      })
      //this.log.info(`set name of selected: '${name}', selected person now: `, selectedPerson)
    },
    setAgeOfSelected: (age: string) => {
      setState({
        selectedPerson: {
          ...state.selectedPerson as FullPerson,
          age: age ? parseInt(age, 10) : 0
        }
      })
    },
    submitPerson: () => {
      const persons = state.persons.map(p => p.id === state.selectedPerson?.id ? {...p, ...state.selectedPerson} : p);
      setState({
        persons
      })
    },  
    post: (post: Post) => {
      const selectedPerson = state.selectedPerson as FullPerson;
      const newSelectedPerson = {...selectedPerson, posts: [...selectedPerson.posts, post]} as FullPerson;
      const persons = state.persons.map(
        p => p.id === state.selectedPerson?.id ? newSelectedPerson : p);
      setState({
        loadPost: false,
        persons,
        selectedPerson: newSelectedPerson
      })
    },
    setLoadPerson: setter<boolean>('loadPerson'),

    setLoadPost: setter<boolean>('loadPost'),
    
    setSelectedDetailsOpen: setter<boolean>('selectedDetailsOpen')
  })
})
