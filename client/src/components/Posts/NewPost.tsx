import Context from "../../Context";
import { FunctionComponent, useContext, useState } from "react";
import styles from './posts.module.sass';
import { FullPerson, Post } from "../../../../types";
import { usePost } from "../../hooks/api";
import { WithTranslation, withTranslation } from "react-i18next";
import { EmbeddedI18n } from "../../base";
import FormComponent from "../Form";
import { Button, TextField, TextareaAutosize } from "@mui/material";

const NewPost: FunctionComponent<WithTranslation> = ({ t }) => {
  const { post, selectedPerson, setLoadPost } = useContext(Context.context);
  const [ title, setTitle] = useState<string>('');
  const [ content, setContent] = useState<string>('');
  const doPost = usePost();
  const submit = async () => {
    setLoadPost(true)
    await new Promise(r => setTimeout(r));
    const personId = (selectedPerson as FullPerson).id
    const { id } = await doPost({
      title, 
      content, 
      personId
    })
    const newPost: Post = { 
      content, 
      title, 
      id, 
      personId 
    }
    setContent('')
    setTitle('')
    await new Promise(r => setTimeout(r, 100));
    post(newPost);
  }
  return (
    <div className={styles.new}>
      <FormComponent title={t('posts.newPostTitle') as string}>
          <TextField
              className="text-input"
              label={t('posts.titleLabel')}
              value={title} onChange={(evt) => setTitle(evt.target.value)}
              variant="standard"
          />
          <TextField
              className="text-input"
              label={t('posts.contentLabel')}
              value={content} onChange={(evt) => setContent(evt.target.value)}
              multiline
              minRows={1}
              maxRows={5}
              variant="standard"
          />
          <EmbeddedI18n
            i18nKey="posts.submit"
            inserts={{
              submit: ({ text }) => <Button 
                variant="contained"
                size="small"
                onClick={submit}>
                  {text}
                </Button>
            }}
          />
        </FormComponent>
    </div>
  )
}

export default withTranslation()(NewPost);