import Context from "../../Context";
import { FunctionComponent, useContext } from "react";
import styles from './posts.module.sass';
import NewPost from "./NewPost";
import Loading from "../Loading";
import { WithTranslation, withTranslation } from "react-i18next";
import Header from "../Header";

const Posts: FunctionComponent<WithTranslation> = ({ t }) => {
    const { selectedPerson, loadPost } = useContext(Context.context);
    return (
    <div key={selectedPerson?.posts.length} className={styles.container}>
        <Header>
            {t('posts.title')}
        </Header>
        {selectedPerson?.posts.length 
        ?
        <> 
            {selectedPerson?.posts.map((post) => {
                return (
                    <div key={post.id} className={styles.post}>
                        <b>{post.title}</b>
                        <pre>{post.content}</pre>
                    </div>
                )
            })}
            {loadPost ? <Loading height={30} /> : null}
        </>
        : <div>-</div>}
        <NewPost />
    </div>
    )
}

export default withTranslation()(Posts);