import { FunctionComponent } from "react";
import { WithTranslation, useTranslation, withTranslation } from "react-i18next";
import styles from './localechooser.module.sass';
import classnames from "classnames";
import { languages } from '../../i18n';

const LocaleChooser:FunctionComponent<WithTranslation> = ({t}) => {
    const { i18n } = useTranslation();
    const choose = (loc:string) => () => {
        i18n.changeLanguage(loc);
    }
    return (
        <div className={styles.container}>
            <label>{t('main.chooseLocale')}:</label>
            {Object.keys(languages).map((loc) => 
                <a 
                    className={classnames({[styles.selected]: loc === i18n.language})}
                    key={loc} 
                    onClick={choose(loc)}>
                        {languages[loc]}
                </a>)}
        </div>
    )
}

export default withTranslation()(LocaleChooser);