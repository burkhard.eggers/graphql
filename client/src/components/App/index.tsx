import { FunctionComponent } from "react";
import styles from './app.module.sass';
import Loading from "../Loading";
import LocaleChooser from "../LocaleChooser";
import {RouterView, useHasRoutedElement} from "../../base";
import Header from "../Header";
import Navigation from "./Navigation";

const App:FunctionComponent = () => {
    const hasOnTop = useHasRoutedElement('ontop')
    return (
        <>
    <div className={styles.container}>
        <div className={styles.header}>
            <div className={styles.headerLabel}>
                <Loading always />
                <LocaleChooser />
                <Navigation />
            </div>
            <Header big>
                <RouterView name="header" />
            </Header>
            <RouterView name="headerMore" />
        </div>
        <div className={styles.body}>
            <RouterView name="main" />
        </div>
        {hasOnTop ? <div className={styles.ontop}>
            <RouterView name="ontop" />
        </div> : null}
     </div>
     </>   
    );
}

export default App;