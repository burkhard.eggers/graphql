import { WithTranslation, withTranslation } from "react-i18next";
import { Link } from "../../base";
import { FunctionComponent } from "react";
import styles from './app.module.sass'

const Navigation: FunctionComponent<WithTranslation> = ({t}) => {
    return (
        <div className={styles.navigation}>
            <Link to="/">{t('main.title')}</Link>
            <Link to="/events">{t('events.title')}</Link>
        </div>
    )
}

export default withTranslation()(Navigation);