import react, { FunctionComponent, PropsWithChildren } from 'react'
import { Typography } from '@mui/material'
import styles from './header.module.sass';

type Props = {
    big?: boolean
}
const Header:FunctionComponent<PropsWithChildren<Props>> = ({ big,  children}) => {
    return (
        <Typography className={styles.container} variant={big ? 'h4' : 'h6'}>
                    {children}
        </Typography>
    )
}

export default Header;