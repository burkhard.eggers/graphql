import React, { FunctionComponent } from "react";
import styles from './loading.module.sass';
import classnames from 'classnames';
import { useIsLoading } from "../../base";

interface Props {
    big?: boolean,
    fill?: boolean,
    height?: number,
    always?: boolean
}
const Loading:FunctionComponent<Props> = ({ big, fill, height, always }) => {
    const loading = useIsLoading();
    const style = () => { 
        const st: any = {}
        if(height) st.height = `${height}px`;
        return st;
    }
    return loading || always ? (
        <div
            style={style()} 
            className={classnames(styles.container, {[styles.big]: big, [styles.fill]: fill})}>
            {loading  ? <img src="/loading.gif" /> : null}
        </div>
    ) : null;
}

export default Loading;