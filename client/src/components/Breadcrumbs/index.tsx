import { FunctionComponent } from "react";
import { Link } from "../../base";
import styles from './breadcrumbs.module.sass';

interface BreadcrumbsEntry {
    path: string,
    label: string
}
type Props = {
    entries: BreadcrumbsEntry[]
}
const Breadcrumbs: FunctionComponent<Props> = ({ entries }) => {
    return (
        <div className={styles.container}>
            {entries.map(({ path, label}) => (
                <span key={path} className={styles.link}>
                    <span>
                        &gt;&gt;
                    </span>
                    <Link to={path}>{label}</Link>
                </span>
               )
            )}
        </div>
    )
}

export default Breadcrumbs;