import { FunctionComponent, PropsWithChildren } from "react";
import styles from './form.module.sass';
import { Box, Typography } from '@mui/material'

interface Props {
    title?: string
}
const FormComponent:FunctionComponent<PropsWithChildren<Props>> = ({ children, title }) => (
    <div
        className={styles.container}>
        {title && (
            <Typography 
                className={styles.title}
                variant="h5">
                {title}
            </Typography>
        )}
        <div className=
        {styles.body}>
            {children}
        </div>
    </div>
)

export default FormComponent;