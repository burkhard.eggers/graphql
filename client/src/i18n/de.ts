export default {
    main: {
        title: 'Personen',
        deleteTooltip: 'löschen',
        deleteConfirm: '{{name}} löschen?',
        idHeader: 'ID',
        nameHeader: 'Name',
        ageHeader: 'Alter',
        nrPostsHeader: 'Anz. Posts',
        chooseLocale: 'Sprache'
    },
    newPerson: {
        title: 'Neue Person',
        nameLabel: 'Name',
        ageLabel: 'Alter',
        submit: 'neue Person {{submit[text:anlegen]}} oder zu {{google}}'
    },
    selectedPerson: {
        title: 'Person {{name}}',
        nameLabel: 'Name',
        ageLabel: 'Alter',
        submit: 'Werte {{submit[text:übernehmen]}}'
    },
    posts: {
        title: 'Posts',
        newPostTitle: 'Neuer Post',
        titleLabel: 'Titel',
        contentLabel: 'Inhalt',
        submit: 'neuen Post {{submit[text:anlegen]}}'
    },
    events: {
        title: 'Ereignisse',
        idLabel: 'ID',
        titleLabel: 'Titel',
        descriptionLabel: 'Beschreibung',
        dateLabel: 'Wann'
    }
}