import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import LanguageDetector from 'i18next-browser-languagedetector';
import en from './en';
import de from './de';

const defaultNS = 'main';

i18n
  .use(initReactI18next)
  .use(LanguageDetector)
  .init({
    ns: [defaultNS],
    defaultNS,
    interpolation: {
      escapeValue: false
    },
    detection: {
      order: ['localStorage', 'sessionStorage', 'navigator'],
    }
  });

  i18n.addResourceBundle('en', defaultNS, en)
  i18n.addResourceBundle('de', defaultNS, de)

  export default i18n;

  export const languages = {
    en: 'english',
    de: 'deutsch'
  } as any