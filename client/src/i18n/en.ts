export default {
    main: {
        title: 'Persons',
        deleteTooltip: 'delete',
        deleteConfirm: 'delete {{name}}?',
        idHeader: 'ID',
        nameHeader: 'Name',
        ageHeader: 'Age',
        nrPostsHeader: 'Nr. Posts',
        chooseLocale: 'language'
    },
    newPerson: {
        title: 'New Person',
        nameLabel: 'Name',
        ageLabel: 'Age',
        submit: '{{submit[text:create]}} new person or go to {{google}}'
    },
    selectedPerson: {
        title: 'Person {{name}}',
        nameLabel: 'Name',
        ageLabel: 'Age',
        submit: '{{submit[text:submit]}} values'
    },
    posts: {
        title: 'Posts',
        newPostTitle: 'new Post',
        titleLabel: 'Title',
        contentLabel: 'Content',
        submit: '{{submit [text:create]}} new post'
    },
    events: {
        title: 'Events',
        idLabel: 'ID',
        titleLabel: 'Title',
        descriptionLabel: 'Description',
        dateLabel: 'When'
    }
}