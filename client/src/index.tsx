import React from 'react';
import ReactDOM from 'react-dom/client';
import Context from './Context';
import apolloClient from './apolloClient';
import i18n from './i18n';
import { Base } from './base';
import App from './components/App';
import routes from './routes';
import './styles.sass';
import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';
import theme from './theme';

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);

root.render(
  <Base
    apolloClient={apolloClient}
    routes={routes}
    theme={theme}
    i18n={i18n}
  >
    <Context.provider>
      <App />
    </Context.provider>
  </Base>
);
