import {
  Event,
  FullPerson,
  Paged,
  PagingInput,
  Person,
  Post,
} from "@/../types";
import useMutation from "../base/hooks/graphql/useMutation";
import useQuery from "../base/hooks/graphql/useQuery";
import { gql } from "@apollo/client";

export const useLoadPersons = () =>
  useQuery<FullPerson[], void>(gql`
    query allPersons {
      persons {
        name
        id
        age
        posts {
          id
        }
      }
    }
  `);

export interface ID {
  id: number;
}

export const useCreatePerson = () =>
  useMutation<ID, Omit<Person, "id">>(gql`
    mutation insertPerson($name: String, $age: Int) {
      insertPerson(person: { name: $name, age: $age }) {
        id
      }
    }
  `);

export const usePost = () =>
  useMutation<ID, Omit<Post, "id">>(gql`
    mutation doPost($title: String, $content: String, $personId: Int) {
      doPost(personId: $personId, content: $content, title: $title) {
        id
      }
    }
  `);

export const useDeletePerson = () =>
  useMutation<void, ID>(gql`
    mutation deletePerson($id: Int) {
      deletePerson(id: $id)
    }
  `);

export const useGetFullPerson = () =>
  useQuery<FullPerson, ID>(gql`
    query person($id: Int!) {
      person(id: $id) {
        id
        name
        age
        posts {
          title
          content
          id
        }
      }
    }
  `);

export const useUpdatePerson = () =>
  useMutation<
    Person,
    {
      personId: number;
      name: string;
      age: number;
    }
  >(
    gql`
      mutation updatePerson($personId: Int, $name: String, $age: Int) {
        updatePerson(id: $personId, person: { name: $name, age: $age }) {
          id
          name
          age
        }
      }
    `
  );

export const useEventsForTable = () =>
  useQuery<Paged<Event>, { paging: PagingInput }>(gql`
    query events($paging: PagingInput) {
      events(paging: $paging) {
        entries {
          date
          description
          title
          id
        }
        totalEntries
      }
    }
  `);
