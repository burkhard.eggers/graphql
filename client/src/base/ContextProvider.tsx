import React, {
  Component,
  Context,
  ReactNode,
} from 'react';
import logger, { Logger } from './logger';

interface ProviderProps {
  children: ReactNode,
}

export default abstract class
ProviderBase<State, Mutators> extends Component<ProviderProps, State> {
  constructor(props: ProviderProps) {
    super(props);
    this.state = this.initialState();
    this.log = logger.get(this.loggerName());
  }
  log:Logger;

  abstract getContext(): Context<State & Mutators>;

  abstract initialState(): State;

  abstract loggerName(): string;

  setter<T>(fieldName: keyof State, logInfoLevel?: boolean) {
    return (cont?: T) => {
      if (cont !== undefined) {
        if(logInfoLevel){
          this.log.info('set', fieldName, cont);
        } else {
          this.log.debug('set', fieldName, cont);
        }
        const newState: State = {
          ...this.state,
          [fieldName]: cont,
        };
        this.setState(newState);
      } else {
        if(logInfoLevel){
          this.log.info('unset', fieldName);
        } else {
          this.log.debug('unset', fieldName);
        }
        const newState: State = {
          ...this.state,
          [fieldName]: undefined,
        };
        this.setState(newState);
      }
    };
  }

  render(): ReactNode {
    // from the state and the mutators we construct the public context
    const {
      state,
      props: { children },
      ...methods
    } = this;
    const { Provider }: {Provider: React.Provider<State & Mutators>} = this.getContext();
    const publicContext: any = { ...state, ...methods };
    delete publicContext.initialState;
    delete publicContext.getContext;
    return (
      <Provider value={publicContext as State & Mutators}>
        {children}
      </Provider>
    );
  }
}
