import { ThemeOptions, useTheme } from "@mui/material";
import logger from "./logger"

const log = logger.get('add-css-variables');

export default (defaultTheme: ThemeOptions, theme: ThemeOptions, scrollbarWidth: number) => {
    log.info(theme, scrollbarWidth);
    const style = document.createElement('style');
    let content = '';
    const yetAdded = [] as string[]
    const addLeafs = (obj: any, prefix: string) => {
        Object.keys(obj).forEach((key) => {
            const val = obj[key]
            if(typeof val === 'string') {
                const name = `${prefix}-${key}`;
                if(yetAdded.indexOf(name) === -1) {
                    content = `${content}
                    ${name}: ${val};`
                    yetAdded.push(name)
                }
            } else addLeafs(val, `${prefix}-${key}`)
        })
    }
    addLeafs(theme, '-');
    addLeafs(defaultTheme, '-');
    const scrollbarVar = `--scroll-bar-width: ${scrollbarWidth}px;` 
    style.innerText = `:root{
        ${scrollbarVar}
        ${content}
    }
        `
    document.head.appendChild(style);
}