import { 
  Component, 
  Context, 
  FunctionComponent, 
  PropsWithChildren, 
  ReactNode, 
  createContext 
} from "react"
import logger, { Logger } from "./logger"

type SetState<State> = (state: Partial<State>) => void

type Setter<State> = <T>(fieldName: keyof State) => (t?: T) => void

type Args<State, Mutators> = {
    initialState: State,
    mutators: (mutatorArgs: { 
      state: State, 
      setState: SetState<State>,
      setter: Setter<State>,
      log: Logger
    }) => Mutators,
    loggerName: string
}

type ContextAndProvider<State, Mutators> = {
    context: Context<State & Mutators>,
    provider: FunctionComponent<PropsWithChildren>
}

export default function<State, Mutators> 
  (args: Args<State, Mutators>): ContextAndProvider<State, Mutators>{
    
    type IContext = State & Mutators;
  
    const context = createContext<IContext>({} as IContext);
  
    class Provider extends Component<PropsWithChildren, State> {
      constructor(props: PropsWithChildren) {
        super(props);
        this.state = args.initialState
        this.log = logger.get(args.loggerName);
      }
      log:Logger;

      render(): ReactNode {
        const {
          state,
          props: { children },
          setState: ss,
          log
        } = this;
        const me = this
        const { Provider }: {Provider: React.Provider<IContext>} = context
        function setter<T>(fieldName: keyof State) {
          return (cont?: T) => {
            if (cont !== undefined) {
              log.info('set', fieldName, cont);
              const newState: State = {
                ...state,
                [fieldName]: cont,
              };
              ss.call(me, newState);
            } else {
              log.info('unset', fieldName);
              const newState: State = {
                ...state,
                [fieldName]: undefined,
              };
              ss.call(me, newState);
            }
          };
        }
        const mutators = args.mutators({
          setState: (st: Partial<State>) => {
              log.info('set state', st)
              ss.call(me, st as any)
          },
          log,
          state,
          setter
        })
        const publicContext: any = { ...state, ...mutators };
        return (
          <Provider value={publicContext as State & Mutators}>
            {children}
          </Provider>
        );
      }
    }

  const provider: FunctionComponent<PropsWithChildren> = ({ children }) => <Provider>{children}</Provider>

    return {
        context,
        provider
    }
}