import context from "../../context";

export interface Confirm {
  msg: string,
  okLabel?: string,
  cancelLabel?: string,
  title?: string,
  callback: (ok: boolean) => void
}

interface State {
  confirm?: Confirm
}

interface Mutators {
  setConfirm: (confirm?: Confirm) => void
}

export default context<State, Mutators>({
  initialState: {
  },
  loggerName: 'modals',
  mutators: ({setter}) => ({
    setConfirm: setter<Confirm>('confirm')
  })
})