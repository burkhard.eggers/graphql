import React, { FunctionComponent, useContext, useEffect} from 'react';
import ModalsContext, { Confirm } from './ModalsContext';
import { Dialog, DialogContent, DialogActions, DialogTitle, Button, IconButton, useTheme } from '@mui/material';
import { CloseSharp } from '@mui/icons-material';
import styles from './modals.module.sass';
import { WithTranslation, withTranslation } from 'react-i18next';
import useEscapeStack from '../../hooks/escStack/useEscapeStack';

const ConfirmDialog: FunctionComponent<WithTranslation> = ({ t }) => {
    const { confirm, setConfirm } = useContext(ModalsContext.context);
    const theme = useTheme()
    const {pushToStack, removeFromStack } = useEscapeStack('confirm');
    const close = (ok?: boolean) => {
        const { callback } = confirm as Confirm;
        setConfirm();
        callback(!!ok);
        removeFromStack();
    }
    useEffect(() => {
        if(confirm) pushToStack(close)
    }, [confirm])
    const onOk = () => {
        close(true)
    }
    const onCancel = () => {
        close(false)
    }
    return (
        <Dialog
            className={styles.confirm}
            open={!!confirm}
            onClose={() => close(false)}
            disableEscapeKeyDown
        >
            {confirm?.title && 
                <DialogTitle className={styles.title}>
                    <IconButton 
                        onClick={onCancel}
                        className={styles.close}>
                        <CloseSharp />
                    </IconButton>
                    {confirm?.title}
                </DialogTitle>}
            <DialogContent className={styles.content}>
                <div 
                    className={styles.sign} 
                    style={{ color: theme.palette.primary.light}}
                >
                    ?
                </div>
                <div className={styles.msg} >
                    {confirm?.msg}
                </div>
            </DialogContent>
            <DialogActions>
                <Button 
                    color="primary" 
                    variant="contained"
                    onClick={onOk}
                >
                    {confirm?.okLabel || t('confirm.okLabel')}
                </Button>
                <Button 
                    color="secondary" 
                    variant="contained"
                    onClick={onCancel}
                    >
                    {confirm?.cancelLabel || t('confirm.cancelLabel')}
                </Button>
            </DialogActions>
        </Dialog>
    )
}

export default withTranslation('components')(ConfirmDialog);