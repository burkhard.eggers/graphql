import { ApolloClient, ApolloProvider } from "@apollo/client";
import { ThemeOptions, ThemeProvider, createTheme, useTheme } from "@mui/material";
import { FunctionComponent, PropsWithChildren, useEffect } from "react";
import GraphQLContext from "../graphQl/GraphQlContext";
import ModalContext from "../Modals/ModalsContext";
import RouterApp from "../router/RouterApp";
import { Route } from "../../types";
import { i18n } from "i18next";
import addModalsNamespace from "../../i18n/addModalsNamespace";
import addCssVariables from "../../addCssVariables";
import '../../global.sass'
import styles from './base.module.sass'
import ScrollMeasure, { scrollbarWidth } from "./ScrollMeasure";

type Props = {
    theme: ThemeOptions,
    apolloClient: ApolloClient<any>,
    routes: Route[],
    i18n: i18n
}

const Base:FunctionComponent<PropsWithChildren<Props>> = 
    ({ children, theme, apolloClient, routes, i18n }) => {
        const defaultTheme = useTheme();
        useEffect(() => {
            addModalsNamespace(i18n)
            addCssVariables(defaultTheme,theme, scrollbarWidth())
        }, [])
    return (
        <ThemeProvider theme={createTheme(theme)}>
            <ScrollMeasure styles={styles}/>
            <ApolloProvider client={apolloClient}>
                <GraphQLContext.provider>
                    <ModalContext.provider>
                        <RouterApp routes={routes}>
                            {children}
                        </RouterApp>
                    </ModalContext.provider>
                </GraphQLContext.provider>
            </ApolloProvider>
        </ThemeProvider>                
    );
}

export default Base;