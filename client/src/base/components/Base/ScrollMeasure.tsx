import { FunctionComponent } from "react";

type Props = {
    styles: any
}

const OUTER_ID = 'IUHIUHIGUIIUREDFJL'
const INNER_ID = 'AWKOKOKOKIUTZRFKL'

const ScrollMeasure:FunctionComponent<Props> = ({ styles}) => {
    return (
        <div id={OUTER_ID} className={styles.scroll}>
            <div id={INNER_ID}>
                inlet
            </div>
        </div>
    )
}

export default ScrollMeasure

export const scrollbarWidth = () => {
    const par = document.getElementById(OUTER_ID)
    const inl = document.getElementById(INNER_ID)
    let scrollbarWidth = 1
    if(par && inl){
        const outerW = par.getBoundingClientRect().width
        const innerW = inl.getBoundingClientRect().width
        scrollbarWidth = outerW - innerW
    }
    return scrollbarWidth
}