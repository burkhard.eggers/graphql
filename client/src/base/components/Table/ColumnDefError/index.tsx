import { FunctionComponent } from "react";
import { WithTranslation, withTranslation } from "react-i18next";

type Props = {
    errors: string[],
    styles: any
}

const ColumnDefError:FunctionComponent<WithTranslation & Props> = ({ errors, styles }) => {
    return <div>{errors[0]}</div>
}

export default withTranslation('components')(ColumnDefError)