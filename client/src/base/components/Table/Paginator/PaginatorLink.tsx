import classNames from "classnames";
import { FunctionComponent, useContext } from "react";
import TableContext from "../TableContext";
import { PageLink } from "./types";
import { Tooltip } from "@mui/material";

type Props = {
    styles: any,
    pageLink: PageLink,
    disabled?: boolean,
    noBorder?: boolean,
    onClick: () => void,
    lastRegular?: boolean
}


const PaginatorLink:FunctionComponent<Props> = 
    ({ styles, pageLink, onClick, disabled, noBorder, lastRegular }) => {
    const {currentPage} = useContext(TableContext)
    return (
        <Tooltip title={pageLink.tooltip}>
            <div
                className={
                    classNames(styles.link, {
                        [styles.selected]: pageLink.page === currentPage,
                        [styles.disabled]: !!disabled,
                        [styles.noBorder]: !!noBorder,
                        [styles.lastRegular]: !!lastRegular
                    })}
                >
                    { disabled 
                        ? pageLink.label 
                        : (
                            <a onClick={onClick}>
                                {pageLink.label}
                            </a>
                        )}
            </div>
        </Tooltip>
    )
}

export default PaginatorLink