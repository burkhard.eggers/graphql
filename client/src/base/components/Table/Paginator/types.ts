import { ReactNode } from "react"

export type PageLink = {
    label: ReactNode,
    tooltip: string,
    page: number
}