import { FunctionComponent, useContext, useEffect, useMemo, useRef, useState } from "react";
import { WithTranslation, useTranslation, withTranslation } from "react-i18next";
import TableContext from "../TableContext";
import logger from "../../../logger";
import PaginatorLink from "./PaginatorLink";
import { PageLink } from "./types";
import { ArrowLeft, ArrowRight, SkipNext, SkipPrevious } from "@mui/icons-material";
import Placeholder from "./Placeholder";
import useResize from "../../../hooks/useResize";
import useForceUpdate from "../../../hooks/useForceUpdate";

const log = logger.get('table-paginator')

type Props = {
    onNavigate: (page: number) => void,
    styles: any
}

const Paginator:FunctionComponent<WithTranslation & Props> = ({
    onNavigate, styles, t
}) => {
    const { currentPage, pageSize, nrPages, paginatedData } = useContext(TableContext)
    const {key, forceUpdate} = useForceUpdate()
    const container = useRef<HTMLDivElement>(null)
    //const [renderNr, setRenderNr] = useState<number>(Date.now())
    let pages: PageLink[] = []
    const getWidth = () => container.current?.getBoundingClientRect().width
    let width = getWidth()
    const maxRecord = paginatedData?.totalEntries || 0;
    const calcPages = () => {
        const w = getWidth()
        if(!w) {
            log.info('no width, try again next tick')
            setTimeout(() => {
                pages = calcPages()
            })
            return []
        }
        const numberLinks = Math.min(Math.floor((w - 200) / 40), nrPages)
        let from = 0;
        if(currentPage >= numberLinks / 2){
            const to = Math.min(Math.floor(currentPage + numberLinks / 2) + 1, nrPages)
            from = to - numberLinks
        } else {
            from = Math.max(Math.floor(currentPage - numberLinks / 2) + 1, 0)
        }
        const size = numberLinks
        log.info(`
            pages ${nrPages}, 
            pageSize ${pageSize}, 
            width ${width}, 
            numberLinks ${numberLinks},
            currentPage ${currentPage},
            `)
        const pgs: PageLink[] = []
        for (let index = from; index < from + size; index++) {
            pgs.push({
                page: index,
                label: `${index + 1}`,
                tooltip: t('table.navigate', 
                {
                    page: index + 1,
                    from: index * pageSize + 1,
                    to: Math.min((index + 1) * pageSize, maxRecord)  
                })
            })
        }

        log.info('paginator links', pgs)
        return pgs;
    }
    pages = useMemo(calcPages, [pageSize, width, currentPage])

    useEffect(() => {
        // after mount, rerender once. we must do this 
        // because pages change otherwise does not lead to rerendering
        forceUpdate()
    }, [])
    const resized = () => {
        // on resize we recalculate pages
        pages = calcPages()
        forceUpdate()
    }
    useResize({
        resized,
        buffer: 200
    })
    const DOTS = true;
    const dotsBefore = pages.length && DOTS && pages[0].page > 0
    const dotsAfter = pages.length && DOTS && pages[pages.length - 1].page < nrPages - 2
    const first = pages.length && pages[0].page > 0
    const previous = currentPage > 0
    const next = currentPage < nrPages - 1
    const last = pages.length && pages[pages.length -1 ].page < nrPages - 1
    const leftDotTooltip = () => t('table.navigateLeftUnshownPages', {
        last: pages[0].page
    })
    const rightDotTooltip = () => t('table.navigateRightUnshownPages', {
        first: pages[pages.length - 1 ].page + 2,
        last: nrPages
    })
    return (
        <div 
            key={key}
            className={styles.paginator} ref={container}>
            <div className={styles.leftPaginator}>
                <PaginatorLink 
                    disabled={!first}
                    styles={styles}
                    pageLink={{
                        page: 0,
                        label: <SkipPrevious />,
                        tooltip: t('table.navigateFirst', 
                        {
                            page: 0,
                            from: 1,
                            to: pageSize  
                        })
                    }}
                    onClick={() => onNavigate(0)}
                    noBorder
                />
                {dotsBefore ? <Placeholder styles={styles} tooltip={leftDotTooltip()} /> : null}
                <PaginatorLink 
                    disabled={!previous}
                    styles={styles}
                    pageLink={{
                        page: currentPage - 1,
                        label: <ArrowLeft style={{transform: 'scale(1.4)'}}/>,
                        tooltip: t('table.navigatePrevious', 
                        {
                            from: (currentPage - 1) * pageSize + 1,
                            to: currentPage * pageSize,
                        })
                    }}
                    onClick={() => onNavigate(currentPage - 1)}
                    noBorder
                />
            </div>
            <div className={styles.middlePaginator}>
                {pages.length && pages.map((pageLink, i) => (
                    <PaginatorLink 
                        styles={styles}
                        key={pageLink.page}
                        pageLink={pageLink}
                        onClick={() => onNavigate(pageLink.page)}
                    />
                    )
                )}
            </div>
            <div className={styles.rightPaginator}>
                <PaginatorLink 
                    disabled={!next}
                    styles={styles}
                    pageLink={{
                        page: currentPage + 1,
                        label: <ArrowRight style={{transform: 'scale(1.4)'}} />,
                        tooltip: t('table.navigateNext', 
                        {
                            from: (currentPage + 1) * pageSize + 1,
                            to: (currentPage + 2) * pageSize,
                        })
                    }}
                    onClick={() => onNavigate(currentPage + 1)}
                    noBorder
                />
                {dotsAfter ? <Placeholder styles={styles} tooltip={rightDotTooltip()} /> : null}
                <PaginatorLink 
                    disabled={!last}
                    styles={styles}
                    pageLink={{
                        page: nrPages - 1,
                        label: <SkipNext />,
                        tooltip: t('table.navigateLast', 
                        {
                            page: nrPages,
                            from: (nrPages - 1) * pageSize + 1,
                            to: Math.min(nrPages * pageSize, maxRecord),
                        })
                    }}
                    onClick={() => onNavigate(nrPages - 1)}
                    noBorder
                />
            </div>
        </div>
    )
}

export default withTranslation('components')(Paginator)