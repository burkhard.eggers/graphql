import { Tooltip } from "@mui/material";
import { FunctionComponent } from "react";
import { WithTranslation, withTranslation } from "react-i18next";

type Props = {
    styles: any
    tooltip: string
}

const Placeholder:FunctionComponent<Props & WithTranslation> = ({ styles, tooltip, t }) => {
    return (
        <div className={styles.placeholder}>
            <Tooltip title={tooltip}>
                <span>.&nbsp;.&nbsp;.</span>
            </Tooltip>
        </div>
    )
}

export default withTranslation('components')(Placeholder)