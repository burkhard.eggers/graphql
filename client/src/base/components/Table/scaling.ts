import { Column } from "./types";
import logger from "../../logger";

const log_ = logger.get('scaling')

export const getColumns = (width: number, columns: Column<any, any>[] | undefined) => {
    return (columns || []).filter(c => {
        const { hideWhenTableWidthLessThan } = c.scale || {}
        return !hideWhenTableWidthLessThan || width >= hideWhenTableWidthLessThan
    });
}
type StyleArg = {
    width: number, 
    column: Column<any, any>, 
    log?: boolean,
    header?: boolean
}
export const getStyle = (arg: StyleArg) => {
    const { column: { id, scale }, width, header} = arg;
    let take: boolean
    let effWidth = 0;
    if(!scale) take = true
    else {
        const {takeRemainingForTableWidth, width: colWidth} = scale
        effWidth = colWidth || 0
        if(!takeRemainingForTableWidth) {
            take = false
            if(header) log_.debug('col', id, 'take', take)
        } else {
            const { from, to } = takeRemainingForTableWidth
            const higherThanFrom = from ? width >= from : true
            const lowerThanTo = to ? width < to : true
            take = higherThanFrom && lowerThanTo 
            if(header) log_.debug('col', id, 'take', take, 'higher than from ', higherThanFrom, 'lower than to', lowerThanTo)
        }
    }
    const sWidth = take ? 'auto' : `${effWidth}px` 
    const st = {
        width: sWidth
    } as any
    if(header) st.maxWidth = take ? '1px' : 'auto'
    return st;
}

export const checkValid = 
    (columns: Column<any, any>[] | undefined, t: any): string[] | undefined => {
    const errors: string[] = []

    if(columns){
        // all columns can't be hidden when they take remaining part
        columns.forEach(col => {
            if(col.scale) {
                const { hideWhenTableWidthLessThan, takeRemainingForTableWidth} = col.scale 
                if(hideWhenTableWidthLessThan 
                    && takeRemainingForTableWidth){
                        const { from, to } = takeRemainingForTableWidth
                    if(!from && to !== undefined && hideWhenTableWidthLessThan < to
                    || from !== undefined && hideWhenTableWidthLessThan > from){
                        errors.push(`column '${col.id}': hidden for less than ${hideWhenTableWidthLessThan}px, but range to take remaining width intersects with that`)
                    }
                }
            }
        })
        // for remaining intervalls must not overlap. And must cover all
        const copy = [...columns.map(c => ({...c}))].filter(c => !!c.scale?.takeRemainingForTableWidth)
        const sortedByRemainingInterval = copy.sort((c1, c2) => {
            const value = (c: Column<any,any>) => {
                const {takeRemainingForTableWidth} = c.scale || {}
                return (takeRemainingForTableWidth?.from || takeRemainingForTableWidth?.to || 0) as number
            }
            const v1 = value(c1)
            const v2 = value(c2)
            if(v1 === v2) return 0
            return v1 > v2 ? 1 : -1
        })
        log_.debug('sorted columns', sortedByRemainingInterval.map(c => c.id))        
        let gFrom = -1
        let gTo = -1
        for (let index = 0; index < sortedByRemainingInterval.length; index++) {
            const column = sortedByRemainingInterval[index];
            log_.debug(`column ${column.id}...`)
            const { from, to } = column.scale.takeRemainingForTableWidth || {}
            if(from){
                log_.debug(`from ${from}...`)
                if(gTo === -1 || gTo === from - 1){
                    gTo = to || -1
                    gFrom = from
                } else {
                    if(gTo < from - 1) {
                        debugger
                        errors.push(`from ${gTo} to ${from - 1} no column covers in its take-remaining interval` )
                    } else {
                        errors.push(`from ${from} to ${gTo} take-remaining intervals overlap` )
                    }
                }
            }
            if(to){
                log_.debug(`from ${to}...`)
                if(gFrom === -1 || gFrom === to + 1){
                    gFrom = from || -1
                    gTo = to
                } else {
                    if(gFrom > to + 1){
                        errors.push(`from ${to + 1} to ${gFrom - 1} no column covers in its take-remaining interval` )
                    } else {
                        errors.push(`from ${gFrom} to ${to} take-remaining intervals overlap` )
                    }
                }
            }
        }
    }
    return errors
}