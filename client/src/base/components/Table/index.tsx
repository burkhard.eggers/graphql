import Table, {Props} from "./GenericTable";
import {Provider} from "./TableContext";
import subTable from "./subTable";

const FlowTable = function<ROW>({pageSize = 100, ...moreProps}: Omit<Props<ROW>, 'rowHeight'>){
    return (
        <Provider>
            <Table<ROW> {...moreProps} pageSize={pageSize} />
        </Provider>
    )
}

export {FlowTable}

export const ScalingTable = function<ROW>(props: Omit<Props<ROW>, 'pageSize'>){
    return (
        <Provider>
            <Table<ROW> {...props} />
        </Provider>
    )
}

export {subTable}