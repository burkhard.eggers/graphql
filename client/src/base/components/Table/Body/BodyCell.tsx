import { PropsWithChildren, useContext } from "react"
import { Column } from "../types"
import TableContext from "../TableContext"
import classNames from "classnames"
import { getStyle } from "../scaling"

type Props<ROW, FIELD> = {
    column: Column<ROW, FIELD>,
    row: ROW
}

function BodyCell<ROW, FIELD>({ row, column }: PropsWithChildren<Props<ROW, FIELD>>){
    const { sortColumn, styles, width } =  useContext(TableContext)
    const field = column.value ? column.value(row) : (row as any)[column.id]
    
    return (
            <td 
                style={getStyle({
                    width,
                    column
                })}
                className={classNames(styles.cell, {
                    [styles.selected]: column.id === sortColumn
                })}>
                    {column.render ? column.render({row, field}) : field}
            </td>
        )

}

export default BodyCell