import { MouseEvent, useContext, useState } from "react";
import TableContext from "../TableContext";
import { ArrowRight, RotateLeft } from "@mui/icons-material";
import { ExpandedRow, RowExpansion } from "../types";
import useRotate from "../../../hooks/useRotate";
import classNames from "classnames";

type Props<ROW> = {
    row: ROW,
    id: string
}

function ExpandLink<ROW>({ id, row }: Props<ROW>){
    const { 
        expandedRows, 
        expanding, 
        startExpanding, 
        expandRow, 
        props, 
        checkScroll,
        beginExpandAnimation,
        endExpandAnimation,
        styles
     } = useContext(TableContext)
    const expanded = expandedRows.has(id) && expandedRows.get(id)?.expanded 
    const [fading, setFading] = useState<boolean>(!!expanded)
    const rotate = useRotate()
    const clicked = async (evt: MouseEvent) => {
        evt.stopPropagation()
        beginExpandAnimation()
        setFading(true)
        if(expandedRows.has(id)){
            const er = expandedRows.get(id) as ExpandedRow
            er.expanded = true
            expandRow(id, er)
        } else {
            startExpanding(id)
            const get = props?.rowExpansion as RowExpansion<ROW>
            const component = await get.component(row)
            const expandedRow = {
                component,
                expanded: true 
            } as ExpandedRow
            expandRow(id, expandedRow)
        }
        endExpandAnimation()
        await new Promise(r => setTimeout(r, 1000))
        setFading(false)
        checkScroll()
    }
    return (
        <span onClick={clicked} className={classNames({[styles.fading]: fading || expanded})}>
            {
            expanding.indexOf(id) > -1 || fading 
                ? <RotateLeft className={rotate} fontSize="small"/>
                : <ArrowRight />
            }
        </span>
    ) 
} 

export default ExpandLink;