import { FunctionComponent, useContext, useEffect, useRef, useState } from "react";
import TableContext from "../TableContext";
import { EmeddableComponentProps, ExpandedRow } from "../types";
import { ArrowDropUpSharp } from '@mui/icons-material' 
import classNames from "classnames";

type Props<ROW> = {
    colSpan: number,
    id: string,
    row: ROW
}

function ExpandedContent<ROW>({ id, colSpan, row }: Props<ROW>){
    const { 
        expandedRows, 
        expanding, 
        expandedHeightKnown, 
        checkScroll, 
        styles,
        collapseRow, 
        beginExpandAnimation,
        endExpandAnimation
    } = useContext(TableContext)
    const [showCollapseLink, setShowCollapseLink] = useState<boolean>(true)
    const wrapper = useRef<HTMLDivElement>(null)
    const er = expandedRows.get(id)
    const shown = !!er
    let Comp: FunctionComponent<EmeddableComponentProps<ROW>> = () => (
        <div style={{display: 'none'}}></div>
    )
    if(shown) Comp = (er as ExpandedRow).component
    const getEmbedded = () => wrapper.current?.firstElementChild?.nextElementSibling
    useEffect(() => {
        getEmbedded()?.classList.add(styles.embeddedObject)
        return () => {}
    }, [Comp])
    const [fading, setFading] = useState<boolean>(false)
    const heightKnown = () => {
        (async () => {
            await new Promise(r => setTimeout(r, 100))
            // measure height...
            if(wrapper.current){
                const emb = getEmbedded()
                if(emb){
                    const height = emb.getBoundingClientRect().height
                    expandedHeightKnown(id, height)
                    //emb.scrollIntoView()
                }
            }
        })()
    }
    let height
    if(er?.expanded) {
        height = er?.height || 0
    } else {
        height = 0
    }
    const collapse = async () => {
        setFading(true)
        beginExpandAnimation()
        collapseRow(id)
        await new Promise(r => setTimeout(r, 1000))
        setFading(false)
        setShowCollapseLink(true)
        checkScroll()
        await new Promise(r => setTimeout(r))
        endExpandAnimation()
    }
    const heightChanged = async () => {
        await heightKnown()
        checkScroll()
    }
    const isExpanding = expanding && expanding.indexOf(id) > -1
    return (
            <tr>
                <td 
                    colSpan={colSpan}
                    className={styles.extendedTd} 
                >
                    <div 
                        ref={wrapper}
                        style={{height}}
                        >
                        <span 
                            className={classNames(styles.collapseRow, {
                                [styles.fading]: fading || isExpanding
                            })}
                            onClick={collapse}
                            >
                            {showCollapseLink && <ArrowDropUpSharp />}     
                        </span>
                        <Comp 
                            row={row} 
                            onInitialHeightKnown={heightKnown} 
                            onHeightChange={heightChanged}
                        />
                    </div>
                </td>
            </tr>
        )
}

export default ExpandedContent