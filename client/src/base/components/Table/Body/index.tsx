import { Fragment, useContext } from "react";
import TableContext from "../TableContext";
import classNames from "classnames";
import logger from "../../../logger";
import BodyCell from "./BodyCell";
import { getColumns } from "../scaling";
import ExpandLink from "./ExpandLink";
import ExpandedContent from "./ExpandedContent";

const log = logger.get('table')

function Body<ROW>(){
    const { 
        props, 
        styles, 
        paginatedData, 
        width, 
        expandedRows, 
        startExpanding,
     } =  useContext(TableContext)
    const keyFromRow = (row: ROW) => props?.rowKey ? props?.rowKey(row) : ''
    const rowClicked = (row: ROW) => () => {
        if(props?.rowClicked){
            const key = props.rowKey(row)
            log.info('row clicked', row, ', key ', key)
            props.rowClicked(key)
        }
    }
    const clickExpand = (row: ROW) => async () => {
        const id = keyFromRow(row);
        startExpanding(id)
        //await new Promise(r => setTimeout(r, 1000))
        //setExpanding(id, false)
    }
    return (
        <tbody>
            {((paginatedData?.entries || []) as ROW[]).map((row, i) => {
                const id = keyFromRow(row)
                const cols = getColumns(width, props?.columns)
                const expanded = expandedRows.get(id)
                const expansion = props?.rowExpansion
                const expandFunction = 
                    (expansion ? expansion.expandable : undefined) || (() => true)
                const expandable = expandFunction(row)
                return (
                    <Fragment key={keyFromRow(row)}>
                        <tr 
                            onClick={rowClicked(row)}
                            className={classNames({
                                [styles.rowClickable]: !!props?.rowClicked,
                                [styles.odd]: i % 2 === 1 && !!props?.markEvenOdd,
                                [styles.even]: i % 2 === 0 && !!props?.markEvenOdd,
                            })}
                        >
                            {props?.rowExpansion && (
                                <td className={styles.expand}>
                                    {expandable && (
                                        <ExpandLink 
                                            row={row} 
                                            id={keyFromRow(row)}
                                        />
                                    )}
                                </td>
                            )}
                            {cols.map(col => <BodyCell<ROW, any> 
                                key={col.id}
                                column={col} 
                                row={row}
                            />)}
                        </tr>
                        {expanded && <ExpandedContent 
                            row={row}
                            id={id}
                            colSpan={1 + cols.length} 
                        />}
                    </Fragment>
                )})}
        </tbody>
        )
}

export default Body;