import { PagedTable, TablePaging, DataLoader, Column } from "./types";

export default <ROW> (rows:ROW[], columns: Column<ROW, any>[]): DataLoader<ROW> => {
    return (paging: TablePaging) => new Promise<PagedTable<ROW>>( res => {
        // sort
        const value = (r: ROW) => {
            const col = columns.find(c => c.id === paging.sortColumn)
            if(col){
                return col.value ? col.value(r) : (r as any)[col.id] 
            }
        }
        const copy = [...rows.map(r => ({...r}))] 
        const sorted = copy.sort((r1, r2) => {
            const v1 = value(r1)
            const v2 = value(r2)
            if(v1 === v2) return 0;
            let compare = 0
            if(v1 !== v2) compare = v1 > v2 ? 1 : -1
            if(!paging.sortAscending) compare *= -1;
            return compare
        })
        const entries = sorted.slice(paging.from, paging.to)
        res({
            entries,
            totalEntries: rows.length
        })
    })
}