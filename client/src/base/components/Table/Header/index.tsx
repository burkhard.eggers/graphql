import { useContext } from "react";
import TableContext from "../TableContext";
import { CProps, Column } from "../types";
import classNames from "classnames";
import { ArrowDownward, ArrowUpward } from "@mui/icons-material";
import { useTranslation } from "react-i18next";
import { Tooltip } from "@mui/material";
import { getStyle, getColumns } from "../scaling";

type Props<ROW> = {
    onSort: (column: string, ascending: boolean) => void,
}

function Header<ROW>({ onSort }: Props<ROW>){
    const { 
        sortColumn, 
        sortAscending,
        setSort,
        props,
        styles,
        width
     } =  useContext(TableContext)
     const {t} = useTranslation('components')
 
    const click = (col: Column<ROW, any>) => async () => {
        let newCol:string | undefined = sortColumn;
        let ascending: boolean = sortAscending;
        if(sortColumn === col.id){
            // toggle sort direction
            setSort(sortColumn, !sortAscending)
            newCol = sortColumn
            ascending = !sortAscending
        } else {
            setSort(col.id, true)
            newCol = col.id
            ascending = true
        }
        await new Promise(r => setTimeout(r))
        onSort(newCol, ascending)
    }

    const renderCell = (col: Column<ROW, any>) => {
        let arrow = null;
        if(sortColumn === col.id){
            arrow = sortAscending ? <ArrowUpward /> : <ArrowDownward />
        }
        let inner;
        if(col.sortable){
            let i18nKey = 'table.sortAscending'
            if(sortColumn === col.id) {
                i18nKey = sortAscending ? 'table.sortDescending' : 'table.sortAscending'
            } 
            const tt = t(i18nKey, {column: col.headerLabel})
            inner = (
                <>
                <Tooltip title={tt}>
                    <a onClick={click(col)}>{col.headerLabel}
                    </a>
                </Tooltip>
                <span>
                    {arrow}
                </span> 
                </>
            ) 
        } else {
            inner = col.headerLabel
        }
        return <div>{inner}</div>
    }
    return (
        <thead>
            <tr>
                {props && (props as CProps).rowExpansion && (
                        <th className={styles.expand}></th>
                    )}
                {getColumns(width, props?.columns).map(column => (
                    <th 
                        style={getStyle({
                            width, 
                            column, 
                            header: true
                        })}
                        className={classNames({
                            [styles.selected]: column.id === sortColumn
                        })}
                        key={column.id}
                    >
                        {renderCell(column)}
                    </th>
                ))}
            </tr>
        </thead>
    )
}

export default Header;