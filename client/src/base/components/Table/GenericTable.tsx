import { 
    PropsWithChildren, 
    useContext, 
    useEffect, 
    useMemo, 
    useRef
} from "react";
import { Column, DataLoader, RowExpansion, TablePaging } from "./types";
import classNames from "classnames";
import logger from "../../logger";
import useResize from "../../hooks/useResize";
import Loading from "../../components/Loading";
import Paginator from "./Paginator";
import TableContext from "./TableContext";
import Header from "./Header";
import Body from "./Body";
import loaderFromData from "./loaderFromData";
import { checkValid } from "./scaling";
import ColumnDefError from "./ColumnDefError";
import { t } from "i18next";

const log = logger.get('table')

export type Props<ROW> = {
    columns: Column<ROW, any>[],
    data: ROW[] | DataLoader<ROW>, 
    className?: string,
    rowKey: (row: ROW) => string,
    markEvenOdd?: boolean,
    rowHeight?: number,
    pageSize?: number,
    rowClicked?: (key: string) => void,
    rowExpansion?: RowExpansion<ROW>,
    onPaginate?: (page: number) => void
}

function Table<ROW,>(props: PropsWithChildren<Props<ROW>>){
    const { 
        currentPage, 
        setCurrentPage, 
        resizing, 
        setResizing,
        loadingData, 
        setLoadingData,
        endLoading,
        sortAscending,
        sortColumn,
        init,
        pageSize,
        setPageSize,
        nrPages,
        props: cprops,
        styles,
        bodyOverflowY,
        scrolling,
        setScrolling
    } = useContext(TableContext)
    const container = useRef<HTMLDivElement>(null)
    const errors = useMemo(() => {
        const e = checkValid(props.columns, t)
        if(!e || e.length === 0) return undefined
        log.error(`bad definition of table columns scale
        ${e.map(err => `\t${err}`)}
        `)
        return e
    }, [])
    const { className, onPaginate  } = props;
    const loaderGiven = useMemo(() => typeof props.data === 'function' , [])
    const loader = useMemo(() => {
    return (
                loaderGiven 
                    ? props.data 
                    : loaderFromData<ROW>(props.data as ROW[], props.columns)
            ) as DataLoader<ROW> 
    }, [props.data])
    const absolute = !!props.rowHeight
    useEffect(() => {
        const cpr = {
            ...props, 
            markEvenOdd: !!props.markEvenOdd, 
            data: undefined, 
            checkScrolling
        }
        init(cpr)
        return () => {}       
    }, [])
    useEffect(() => {
        (container.current as any).data = {
            sortColumn, currentPage, sortAscending, pageSize
        }
        return () => {}       
    }, [sortColumn, currentPage, sortAscending, pageSize])
    const reloadCurrentSorting = () => {
        const { data } = container.current || {} as any
        if(!data) return
        log.info('data', data)
        setCurrentPage(0)
        loadCurrentPage(0, data.sortColumn, data.sortAscending)
    }
    const onResizing = () => {
        log.info(resizing)
        setResizing(true)
    }
    useResize({
        resized: reloadCurrentSorting,
        resizing: onResizing,
        buffer: 300
    });
    useEffect(() => {
        if(!loaderGiven) reloadCurrentSorting()
        return () => {}       
    }, [props.data])
    const loadCurrentPage = async (page: number, sortColumn: string, sortAscending: boolean) => {
        const {rowHeight} = props
        let size = 0
        log.info('absolute', absolute, ', size', size, ', current page', page)
        if(absolute && rowHeight && container.current){
            let headerHeight = 
                container.current.querySelector(`.table-header`)
                    ?.getBoundingClientRect().height || 28
            const footerHeight = 
                container.current.querySelector('.table-footer > div')
                ?.getBoundingClientRect().height || 24 
            log.info('header height', headerHeight, 'footer height', footerHeight)    
            const hfHeight = Math.floor(headerHeight + footerHeight)
            const tableHeight = container.current.getBoundingClientRect().height
            const bodyHeight = tableHeight - hfHeight;
            size = Math.floor(bodyHeight / (rowHeight + 5))
            log.info('table height', tableHeight, 'body height', bodyHeight, 'pageSize', size)
            setPageSize(size)
            setLoadingData(true)
            log.info('load page', page)
        } else if(!rowHeight){
            size = props.pageSize || 0
            setLoadingData(true)
        }
        const from = page * size
        const to = from + size
        const paging = { 
            from,
            to,
            sortAscending,
            sortColumn
        } as TablePaging
        const rows = await loader(paging)
        const width = container.current?.getBoundingClientRect().width || 0
        log.info('paging', paging, 'data', rows, 'width', width)
        endLoading(rows, size, page, width)
        if(absolute){
            await new Promise(r => setTimeout(r))
            checkScrolling()
        }
    }
    const checkScrolling = () => {
        const body = container.current?.querySelector(`.${styles.body}`)
        const table = body?.querySelector('table')
        const bodyHeight = body?.getBoundingClientRect().height || 0;
        const tableHeight = table?.getBoundingClientRect().height || bodyHeight;
        const isscrolling = bodyHeight < tableHeight
        log.info('scrolling', isscrolling, bodyHeight, tableHeight)
        setScrolling(isscrolling)
    }
    const activatePage = async (page: number) => {
        if(onPaginate) onPaginate(page)
        setCurrentPage(page)
        await new Promise(r => setTimeout(r))
        log.info('set page', page, 'size', pageSize, 'current', currentPage)
        loadCurrentPage(page, sortColumn as string, sortAscending)
    }
    const sortAfterHeaderClick = (sortColumn: string, sortAscending: boolean) => {
        setCurrentPage(0)
        log.info('resort, asc=', sortAscending, ', column=', sortColumn)
        loadCurrentPage(0, sortColumn, sortAscending)
    }

    const renderAbsoluteLayout = () => {
        const style = {} as any
        if(bodyOverflowY) style.overflowY = bodyOverflowY
        return (
        <>
            <div className={styles.header}>   
                <table className="table-header">
                    <Header<ROW> 
                        key={`${loadingData}`} 
                        onSort={sortAfterHeaderClick}
                    />
                </table>
            </div>
            <div className={styles.body} style={style}>
                    {loadingData 
                        ? <Loading area/>
                        : (
                    <table>
                        <Body<ROW> />
                    </table>
                        )}
            </div>
            {nrPages > 1 && !loadingData &&  (
                <div className={classNames(styles.footer, 'table-footer')}>
                    <div>
                        <Paginator 
                            styles={styles}
                            onNavigate={activatePage}
                        />
                    </div>
                </div>
            )}
        </>
        )
     }
    const renderFlowLayout = () => {
        if(loadingData) {
            const loadingHeight = 23 * (props.pageSize || 0) + 28 + 24
            return (
                <div 
                    className={styles.flowLoading}
                    style={{height: `${loadingHeight}px`}}>
                    <Loading area/>
                </div>
            ) 
        }
        return (
            <>
                <table>
                    <Header<ROW> onSort={sortAfterHeaderClick}/>
                    <Body<ROW> />
                </table>
                {nrPages > 1 && (
                    <div className={classNames(styles.footer, 'table-footer')}>
                        <div>
                            <Paginator 
                                styles={styles}
                                onNavigate={activatePage}
                            />
                        </div>
                    </div>
                )}
            </>
        )
    }
    return (
        <div 
            ref={container}
            className={classNames(
                styles.container, 
                    {
                        [styles.absolute]: absolute,
                        [styles.flow]: !absolute,
                        [styles.scrolling]: scrolling,
                        [styles.resizing]: resizing && !loadingData,
                        [className as string]: !!className
                    }
                )}
            >
            {errors && <ColumnDefError errors={errors} styles={styles} />}
            {!errors && (absolute ? renderAbsoluteLayout() : renderFlowLayout())}
        </div>
    )
}

export default Table

