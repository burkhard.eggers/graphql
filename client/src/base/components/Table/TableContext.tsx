import {
    createContext,
    Context,
  } from 'react';
  import ContextProvider from '../../ContextProvider';
import { CProps, ExpandedRow, PagedTable } from './types';
import styles from './table.module.sass'
  
  interface State {
    styles: any, 
    currentPage: number,
    paginatedData?: PagedTable<any>,
    resizing: boolean,
    loadingData: boolean,
    sortAscending: boolean,
    sortColumn?: string,
    initiated: boolean,
    props?: CProps,
    pageSize: number,
    nrPages: number,
    width: number,
    expandedRows: Map<string, ExpandedRow>,
    expanding: string[],
    bodyOverflowY: string,
    scrolling: boolean
  }
  
  interface Mutators {
    setCurrentPage: (currentPage: number) => void,
    setPaginatedData: (paginatedData: PagedTable<any>) => void
    setResizing: (resizing: boolean) => void,
    setLoadingData: (loadingData: boolean) => void,
    endLoading: (
      paginatedData: PagedTable<any>, 
      pageSize: number, 
      currentPage: number,
      width: number
      ) => void,
    setSort: (sortColumn: string, sortAscending: boolean) => void,
    init: (props: CProps) => void,
    setPageSize: (pageSize: number) => void,
    expandRow: (id: string, er: ExpandedRow) => void,
    collapseRow: (id: string) => void, 
    checkScroll: () => void,
    startExpanding: (id: string) => void,
    expandedHeightKnown: (id: string, height: number) => void,
    setBodyOverflowY: (bodyOverflowY: string) => void,
    beginExpandAnimation: () => void,
    endExpandAnimation: () => void,
    setScrolling: (scrolling: boolean) => void
  }
  
  type IContext = State & Mutators;
  
  const TableContext = createContext<State & Mutators>({} as IContext);
  
  export class Provider<ROW> extends ContextProvider<State, Mutators> implements Mutators{
    /* *********** initial state *************************** */
    initialState(): State {
      return {
        styles,
        pageSize: 0,
        currentPage: 0,
        resizing: false,
        loadingData: false,
        sortAscending: true,
        sortColumn: undefined,
        initiated: false,
        nrPages: 0,
        width: 0,
        expandedRows: new Map<string, ExpandedRow>(),
        expanding: [],
        bodyOverflowY: 'auto',
        scrolling: false
      };
    }
  
    /* *************** logger name ************************* */
    loggerName(): string {
      return 'table-context';
    }
  
    /* *************** define the context instance ********* */
    getContext(): Context<IContext> {
      return TableContext;
    }
  
    /* *************** mutators **************************** */
    setCurrentPage = (currentPage: number) => {
      this.setState({
        currentPage,
        expandedRows: new Map<any,any>()
      })
    }
    
    setPaginatedData = this.setter<PagedTable<any>>('paginatedData', true)

    setResizing = this.setter<boolean>('resizing', true)

    setLoadingData = this.setter<boolean>('loadingData', true)

    setPageSize = this.setter<number>('pageSize', true)

    endLoading = (
      paginatedData: PagedTable<any>, 
      pageSize: number, 
      currentPage: number,
      width: number
      ) => {
      this.setState({
        loadingData: false,
        resizing: false,
        paginatedData, 
        currentPage,
        pageSize,
        width,
        nrPages: Math.floor(paginatedData.totalEntries / (pageSize || 1) + 0.9),
      })
    }

    setSortAscending = this.setter<boolean>('sortAscending', true)

    setSortColumn = this.setter<string>('sortColumn', true)

    init = (props: CProps) => {
      this.log.info('init', props)
      this.setState({
        props,
        pageSize: props.pageSize || 0
      })
    }
    setSort = (sortColumn: string, sortAscending: boolean) => {
      this.log.info('set sort, column = ', sortColumn, ', ascending = ', sortAscending)
      this.setState({
        sortColumn,
        sortAscending
      })
    }

    expandRow = async (id: string, er: ExpandedRow) => {
      this.log.info(`expand row ${id}`, er)
      const { expandedRows } = this.state
      const newExpandedRows = new Map<string, ExpandedRow>(expandedRows)
      er.expanded = true
      newExpandedRows.set(id, er)
      this.setState({
        expandedRows: newExpandedRows
      })
    }

    collapseRow = (id: string) => {
      this.log.info(`collapse row ${id}`)
      const { expandedRows} = this.state
      if(expandedRows.has(id)) {
        const newExpandedRows = new Map<string, ExpandedRow>(expandedRows)
        const er = newExpandedRows.get(id) as ExpandedRow
        er.expanded = false
        this.setState({
          expandedRows: newExpandedRows
        })  
      } 
    }

    checkScroll = () => {
      this.state.props?.checkScrolling()
    }

    startExpanding = (id: string) => {
      this.log.info(`expanding row ${id}`)
      if(this.state.expanding.indexOf(id) > -1) return
      this.setState({
        expanding: [...this.state.expanding, id]
      })
    }

    expandedHeightKnown = (id: string, height: number) => {
      this.log.info(`expanded height known ${height} for id ${id}`)
      const { expandedRows, expanding } = this.state
      const newExpandedRows = new Map<string, ExpandedRow>(expandedRows)
      const er = expandedRows.get(id) as ExpandedRow
      er.height = height
      newExpandedRows.set(id, er)
      const newExpanding = [...expanding].filter(e => e !== id)
      this.setState({
        expandedRows: newExpandedRows,
        expanding: newExpanding
      })
    }

    setBodyOverflowY = this.setter<string>('bodyOverflowY', true)

    beginExpandAnimation = () => {
      if(this.state.scrolling){
        this.setState({
          bodyOverflowY: 'scroll'
        })
      } else {
        this.setState({
          bodyOverflowY: 'hidden'
        })
      }

    }

    endExpandAnimation = () => {
      this.setState({
        bodyOverflowY: 'auto'
      })
    }

    setScrolling = this.setter<boolean>('scrolling', true)
  }
  
  export default TableContext;
  