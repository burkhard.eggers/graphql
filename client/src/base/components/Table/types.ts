import { Component, FunctionComponent, ReactElement, ReactNode } from "react"

export type ColumnRenderer<ROW,V> = FunctionComponent<{row: ROW, field: V}>

export type ColumnScale = {
    width?: number,
    hideWhenTableWidthLessThan?: number,
    takeRemainingForTableWidth?: {
        from?: number,
        to?: number,
    }
}
export interface Column<ROW, V> {
    headerLabel: string,
    id: string,
    className?: string,
    value?: (row: ROW) => V,
    sortable?: boolean | ((v: V) => string), 
    render?:  ColumnRenderer<ROW,V>,
    scale: ColumnScale
}

export type ExpandedRow = {
    component: FunctionComponent<EmeddableComponentProps<any>>,
    height?: number,
    expanded: boolean
  }
export type EmeddableComponentProps<ROW> = {
    row: ROW,
    onHeightChange?: () => void,
    onInitialHeightKnown: () => void
} 

export type RowExpansion<ROW> = {
    component: (row: ROW) => Promise<FunctionComponent<EmeddableComponentProps<ROW>>>,
    expandable?: (row: ROW) => boolean
}
export type CProps = {
    columns: Column<any, any>[], 
    rowKey: (row: any) => string,
    rowHeight?: number,
    pageSize?: number,
    markEvenOdd: boolean,
    rowClicked?: (key: string) => void,
    rowExpansion?: RowExpansion<any>,
    checkScrolling: () => void
}

export type TablePaging = {
    from: number, 
    to: number, 
    sortAscending: boolean, 
    sortColumn: string
}

export type PagedTable<ROW> =  {
    entries: ROW[],
    totalEntries: number
  }

export type DataLoader<ROW> = (paging: TablePaging) => Promise<PagedTable<ROW>> 

export type Subtable<PARENT, CHILD> = {
    title?: (parent:PARENT) => ReactNode,
    empty?: (parent:PARENT) => ReactNode,
    rowKey: (row:CHILD) => string,
    columns: Column<CHILD, any>[],
    markEvenOdd?: boolean,
    getData: (row: PARENT) => Promise<CHILD[]>,
    pageSize?: number,
    expandable?: (parent:PARENT) => boolean
}