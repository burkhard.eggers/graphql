import { FunctionComponent, useContext, useEffect } from "react"
import { EmeddableComponentProps, RowExpansion, Subtable  } from "./types"
import { FlowTable } from "."
import TableContext from "./TableContext"
import classNames from "classnames"

const subTable = function<PARENT, CHILD>(sub:Subtable<PARENT, CHILD>): RowExpansion<PARENT>{
    const rowExpansion = {
        component: (parent: PARENT) => new Promise<FunctionComponent<EmeddableComponentProps<PARENT>>>(async res => {
            const children = await sub.getData(parent)
            const hasFooter = children.length > (sub.pageSize || 10)
            const Embedded: FunctionComponent<EmeddableComponentProps<PARENT>> = 
            ({ row, onInitialHeightKnown, onHeightChange }) => {
                useEffect(() => {
                    onInitialHeightKnown()
                    return () => {}
                }, [])
                const {styles} = useContext(TableContext)
                const onPaginate = () => {
                    if(onHeightChange) {
                        onHeightChange()
                    }
                }
                const empty = children.length === 0 && sub.empty && (
                    <div className={styles.emptySubTable}>
                        {sub.empty(row)}
                    </div>
                )
                return (
                    <div>
                    {sub.title && (
                        <div className={styles.subTableTitle}>
                            {sub.title(row)}
                        </div>
                    )}
                    {empty || (
                    <FlowTable 
                        className={classNames(
                            styles.subTable, {
                            [styles.hasFooter]: hasFooter
                        })}
                        markEvenOdd={!!sub.markEvenOdd}
                        columns={sub.columns}
                        rowKey={sub.rowKey}
                        data={children}
                        pageSize={sub.pageSize}
                        onPaginate={onPaginate}
                    />
                    )}
                    </div>
                )
            }
            res(({onInitialHeightKnown, onHeightChange, row}) => (
                <Embedded 
                    row={row}
                    onInitialHeightKnown={onInitialHeightKnown}
                    onHeightChange={onHeightChange}
                />
                )) 
        }),
        expandable: (row: PARENT) => !!(sub.expandable && sub.expandable(row))
    }
    return rowExpansion
}

export default subTable