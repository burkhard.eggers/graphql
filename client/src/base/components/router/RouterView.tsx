import React, { FunctionComponent, useContext } from 'react';
import {
    Route as RRRoute,
    Routes
  } from "react-router-dom";
import { Route } from '../../types';
import RouterContext from './RouterContext';
import logger from '../../logger';

const log = logger.get('router');
    
interface Props { 
   name: string
}
  const RouterView:FunctionComponent<Props> = ({ name }) => {
    const { routes } = useContext(RouterContext.context);
    if(!routes) return <span>...</span>
    const routes_ = (routes as Route[]).map(({path, content}) => {
      return (
        <RRRoute
          key={path}
          path={path}
          element = {content[name] || ''}
        />
      )
    })
    log.debug(`router view '${name}'`, 
    'components', 
    (routes as Route[]).map(({path, content}) => ({path, component: content[name]})))
    return (
        <Routes>
          {routes_}
        </Routes>
      );
  }

  export default RouterView;