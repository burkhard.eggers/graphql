import context from '../../context';
import { Route } from '../../types';
  
interface State {
  routes?: Route[]
}

interface Mutators {
  setRoutes: (routes: Route[]) => void
}
 

export default context<State, Mutators>({
  initialState: {},
  loggerName: 'router',
  mutators: ({ setter }) => ({
    setRoutes: setter<Route[]>('routes')
  }) 
})
