import React, { FunctionComponent, PropsWithChildren, useContext, useEffect } from 'react';
import {
    createBrowserRouter,
    RouterProvider,
  } from "react-router-dom";
import RouterContext from './RouterContext';
import { Route } from '../../types';
import Modals from '../Modals';
import logger from '../../logger';

const log = logger.get('router');

type Props = {
  routes: Route[]
} 

type PWC = PropsWithChildren<Props>

const RouterApp:FunctionComponent<PWC> = ({ children, routes }) => {
    const { setRoutes } = useContext(RouterContext.context);
    const Elm = <>
    <Modals />
    {children}
    </>
    const router = createBrowserRouter([{
        path: '*',
        element: Elm 
    }]);
    useEffect(() => {
      log.info('routes', routes)
      setRoutes(routes);
    }, [])
      return (
          <RouterProvider router={router} />
      );
  }  

  export default (props:PWC) => <RouterContext.provider><RouterApp {...props}/></RouterContext.provider>;