import { Button, Dialog, DialogActions, DialogContent, DialogTitle, IconButton } from "@mui/material";
import { FunctionComponent, PropsWithChildren, ReactElement, useState } from "react";
import styles from './routingdialog.module.sass'
import { CloseSharp, RotateLeft } from "@mui/icons-material";
import useNavigate from "../../hooks/escStack/useNavigate";
import useRotate from "../../hooks/useRotate";

type Props = {
    onOk?: () => Promise<void>,
    onCancel?: () => Promise<void>,
    title?: ReactElement,
    closeIcon?: boolean,
    backUrl?: (url: string) => string
}
const RoutedDialog:FunctionComponent<PropsWithChildren<Props>> = 
({ children, onOk, onCancel, title, closeIcon, backUrl }) => {
    const rotate = useRotate();
    const [cancelWait, setCancelWait] = useState<boolean>()
    const [okWait, setOkWait] = useState<boolean>()
    const navigate = useNavigate();
    const close = () => {
        if(backUrl) {
            const url = location.pathname
            navigate(backUrl(url))
        } else {
            history.back()
        }
        setOkWait(false)
        setCancelWait(false)
    }
    const okClicked = async () => {
        setOkWait(true)
        if(onOk){
            await onOk();
        }
        close()
    }

    const cancelClicked = async () => {
        setCancelWait(true)
        if(onCancel){
            await onCancel();
        }
        close()
    }

    const onClose = () => {
        if(onCancel) {
            cancelClicked()
        } else close()
    }

    const renderOk = () => (
        <Button 
            color="primary" 
            variant="contained"
            onClick={okClicked}
            size="small"
        >
            {okWait ? <RotateLeft className={rotate} /> : 'ok'}
        </Button>
    );
    const renderCancel = () => (
        <Button 
            color="secondary" 
            variant="contained"
            onClick={cancelClicked}
            size="small"
        >
            {cancelWait ? <RotateLeft className={rotate} /> : 'cancel'}
        </Button>
    )
    return <Dialog
            className={styles.container}
            open
            onClose={onClose}
            >
                {title && (
                    <DialogTitle className={styles.title}>
                        {(closeIcon !== false) && (
                        <IconButton 
                            onClick={cancelClicked}
                            className={styles.close}>
                            <CloseSharp />
                        </IconButton>
                        )}
                        {title}
                    </DialogTitle>
                )}
                <DialogContent>
                    {children}
                </DialogContent>
                { (!!onOk || !!onCancel) ? (
                <DialogActions>
                    {onOk && renderOk()}
                    {onCancel && renderCancel()}
                </DialogActions>
            ) : null}
            </Dialog>
}

export default RoutedDialog;