import { FunctionComponent } from "react";
import classNames from "classnames";
import styles from './loading.module.sass'
import useRotate from "../../hooks/useRotate";
import { RotateLeft } from "@mui/icons-material";

type Props = {
    area: boolean
}
const Loading: FunctionComponent<Props> = ({ area }) => {
    const rotate = useRotate()
    return (
            <div className={classNames(styles.container, {
                [styles.area]: area
            })}>
                <RotateLeft className={rotate} />
            </div>
        )
}

export default Loading