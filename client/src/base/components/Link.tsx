import { FunctionComponent } from "react";
import { LinkProps } from "react-router-dom";
import { Link } from "react-router-dom";
import {getDecoratedUrl} from "../hooks/escStack/useEscapeStack";
type Props = LinkProps & React.RefAttributes<HTMLAnchorElement>
const LinkComponent:FunctionComponent<Props> = (props) => {
    const to = getDecoratedUrl(props.to.toString())
    return <Link {...props} to={to} />
}

export default LinkComponent