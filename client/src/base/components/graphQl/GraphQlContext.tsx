import context from "../../context"

  interface State {
    loading?: boolean
  }
  
  interface Mutators {
    setLoading: (loading: boolean) => void
  }
  
  export default context<State, Mutators>(
    {
      initialState: {
        loading: false
      },
      loggerName: 'graphQL',
      mutators: ({setter}) => ({
        setLoading: setter<boolean>('loading') 
      })
    }
  )