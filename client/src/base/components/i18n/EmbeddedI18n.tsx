import { FunctionComponent, ReactNode } from "react";
import { useTranslation } from "react-i18next";

type ComponentOrString = React.FunctionComponent<any> | React.ReactNode

interface Props {
    i18nKey: string,
    inserts: { [key: string]: ComponentOrString  }
}
const EmbeddedI18n:FunctionComponent<Props> = ({ inserts, i18nKey }) => {
    const { t } = useTranslation();
    const pattern = t(i18nKey);
    const sep = '|@|';
    // split pattern into parts, that can be embedded component or just text. 
    // embedded components will start with {{ and end with }}
    const parts = pattern.replace(/\{\{/gi, `${sep}{{`).replace(/\}\}/gi, `}}${sep}`).split(sep)
    return (
    <>
        {parts.map((part, index) => {
            if(part.startsWith('{{') && part.endsWith('}}')) {
                // embedded component or text
                const nameAndProps = part.substring(2, part.length -2)
                if(nameAndProps.indexOf('[') === -1){
                    //simple text to embed
                    const value = inserts[nameAndProps] as string
                    return <span key={index}>{value}</span>
                } else {
                    // component to embed
                    // name of component is given together with props
                    let props = {} as { [key: string]: string }
                    let Comp: FunctionComponent<any>;
                    // extract name...
                    const name = (nameAndProps.match(/^([^\[]+)/gi) as RegExpMatchArray)[0];
                    // ..and the list of properties
                    let propsList = nameAndProps.substring(name.length + 1, nameAndProps.length - 1).split('\s*,\s*');
                    Comp = inserts[name.trim()] as FunctionComponent;
                    // get props object from propList
                    propsList.forEach(elm => {
                        if(elm.indexOf(':') > -1){  
                            const [k,v] = elm.split(':');
                            props[k.trim()] = v.trim()
                        }
                    })
                    //debugger;
                    // now we have both the component and its properties and we can render it
                    return <Comp key={index} {...props} />
                }
            }
            // part is just text, render it
            return <span key={index}>{part}</ span>
        })}
    </>
    );
}

export default EmbeddedI18n;