import { useState } from "react"

const useForceUpdate = () => {
    const [renderNr, setRenderNr] = useState<number>(Date.now())

    const forceUpdate = () => {
        setRenderNr(Date.now())
    }
    return {key: renderNr, forceUpdate}
}

export default useForceUpdate