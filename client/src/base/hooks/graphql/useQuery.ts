import { TypedDocumentNode, useLazyQuery } from "@apollo/client";
import GraphQLContext from "../../components/graphQl/GraphQlContext";
import { useContext } from "react";
import logger from "../../logger";

const log = logger.get('use query');

export default <T, A>(query: TypedDocumentNode<any>) => {
    const [lazy] = useLazyQuery(query);
    const { setLoading} = useContext(GraphQLContext.context)
    return (variables?: A) => new Promise<T>( async (res) => {
        setLoading(true)
        log.info('call query', query.loc?.source.body, 'variables', variables)
        const d = await lazy(variables ? { variables } : undefined);
        await new Promise(r => setTimeout(r, 500))
        setTimeout(() => setLoading(false), 10);
        const isObj = !!d.data && typeof d.data === 'object';
        const val = isObj ? Object.values(d.data)[0] : d.data
        log.info('result', val)
        res(val);
    })
} 