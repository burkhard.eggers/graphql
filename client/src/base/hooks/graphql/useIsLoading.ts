import { useContext } from "react"
import GraphQLContext from "../../components/graphQl/GraphQlContext"

export default () => {
    const { loading } = useContext(GraphQLContext.context)
    return loading;
}