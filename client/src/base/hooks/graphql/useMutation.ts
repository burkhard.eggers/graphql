import { TypedDocumentNode, useMutation } from "@apollo/client";
import { useContext } from "react";
import GraphQLContext from "../../components/graphQl/GraphQlContext";
import logger from "../../logger";

const log = logger.get('use mutation');

export default <T,A>(mutation: TypedDocumentNode<any>) => {
    const [lazy] = useMutation(mutation);
    const { setLoading} = useContext(GraphQLContext.context)
    return (variables?: A) => new Promise<T>( async (res) => {
        setLoading(true);
        await new Promise(r => setTimeout(r, 500))
        log.info('call mutation', mutation.loc?.source.body, 'variables', variables)
        const d = await lazy(variables ? { variables } : undefined);
        setTimeout(() => setLoading(false), 10);
        const isObj = !!d.data && typeof d.data === 'object';
        const val = isObj ? Object.values(d.data)[0] : d.data
        log.info('result', val)
        res(val);
    })
}