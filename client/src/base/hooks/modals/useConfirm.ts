import { useContext} from 'react'
import ModalsContext, { Confirm } from "../../components/Modals/ModalsContext"

export default () => {
    type Conf = Omit<Confirm, 'callback'>
    const {setConfirm} = useContext(ModalsContext.context);
    return (confirm: Conf) => new Promise<boolean>(callback => {
        setConfirm({
            ...confirm,
            callback
        });
    }) 
}