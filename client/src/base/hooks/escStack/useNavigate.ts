import { useNavigate } from "react-router-dom";
import { getDecoratedUrl } from "./useEscapeStack";
import logger from "../../logger";

const log = logger.get('use navigation');

export default () => {
    const navigate = useNavigate();
    return (path: string) => {
        const url = getDecoratedUrl(path)
        log.info(`path ${path} -> ${url}`)
        navigate(url)
    }
}