import logger from "../../logger"

const log = logger.get('escape-stack')

export type Callback = () => void

export type StackElement = {
    id: string,
    callback: Callback
} 

let stack = [] as StackElement[]

let listening = false;

const PARAM_NAME = 'esc-stack';

export const getDecoratedUrl = (url: string) => {
    if(stack.length === 0 ) return url;
    return `${url}?${PARAM_NAME}=${stack.length}`
}

const getNrFromURL = (url: URL): number => {
    const param = url.searchParams.get(PARAM_NAME)
    if(!param) return 0;
    return parseInt(param, 10)
}

const setNrInURL = (nr: number, ) => {
    const { search } = new URL(location.href);
    const searchParams = new URLSearchParams(search);
    if(nr === 0){
        searchParams.delete(PARAM_NAME)
    } else { 
        searchParams.set(PARAM_NAME, `${nr}`);
    }
    const newSearch = searchParams.toString();
    if(`?${newSearch}` !== search) {
        const url = new URL(location.href)
        url.search = newSearch;
        history.pushState({}, '', url.toString())
    }
}

export default ( id: string) => {
    const location = window.location
    const actionOfLatestOnStack = () => {
        if(stack.length === 0) {
            log.info('action but stack empty')
            return;
        }
        const last = stack.pop();
        if(last){
            setNrInURL(stack.length)
            log.info('action ', last, 'remain elements', stack.length)
            last.callback();
        }
    }
    if(!listening){
        listening = true;
        log.info('prepare listening...', location.hash)
        setNrInURL(0);
        window.document.addEventListener("keydown", (evt) => {
            if(evt.key === 'Escape') {
                log.info('esc hit')
                actionOfLatestOnStack();
            }
        }, false);
        window.onpopstate = (event) => {
            log.info('popstate', event.state)
            setTimeout(() => {
                if(getNrFromURL(new URL(location.href)) === stack.length -1){
                    actionOfLatestOnStack()
                }
            })
        };
    }
    const pushToStack = (callback: Callback ) => {
        stack = stack.filter(s => s.id !== id); 
        stack.push({ id, callback } as StackElement)
        setNrInURL(stack.length)
        log.info(`push id '${id}', stacked elements now: ${stack.length}`)
    } 

    const removeFromStack = () => {
        stack = stack.filter(s => s.id !== id);
        setNrInURL(stack.length)
        log.info(`remove id '${id}', stacked elements now: ${stack.length}`)
    }

    return { pushToStack, removeFromStack }
}