import { useContext } from "react";
import RouterContext from "../components/router/RouterContext";
import { RouteObject, useRoutes } from "react-router-dom";

const useHasRoutedElement = (name: string): boolean => {
    const { routes } = useContext(RouterContext.context);
    const rrRoutes = (routes || []).map(({path, content}) => {
            if(content[name]) return {
                path,
                element: content[name]
            }; 
            return null
        }
    ).filter(rrr => !!rrr) as RouteObject[];
    return !!useRoutes(rrRoutes, location) 
}

export default useHasRoutedElement;