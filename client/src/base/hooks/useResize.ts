import { useEffect, useState } from "react"
import logger from "../logger"

const log = logger.get('useResize')

type RES = () => void
type ARG = {
    resized: RES,
    resizing?: RES,
    buffer?: number,
    disable?: boolean
}
export default ({ resized, resizing, buffer = 1500, disable}: ARG) => {
    let last = Date.now()
    let timeout: any = undefined
    const onResize = () => {
        if(resizing) resizing()
        timeout = setTimeout(() => {
            const diff = Date.now() - last;
            if(diff <= buffer){
                log.debug('diff to short', diff, '-> resizing')
                if(timeout) {
                    clearTimeout(timeout)
                    timeout = undefined
                    //last = Date.now() - 5
                }
            } else {
                last = Date.now()
                log.debug('resized!')
                resized();
            }
        }, buffer)
    }
    useEffect(() => {
        resized()
        if(!disable) {
            addEventListener("resize", onResize);
            return () => {
                removeEventListener("resize", onResize);
            }
        }
    }, [])
}