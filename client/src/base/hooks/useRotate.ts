import { createStyles, makeStyles } from "@mui/styles";

const useRotate = () => makeStyles(() =>
  createStyles({
    rotateIcon: {
      animation: "$spin 2s linear infinite"
    },
    "@keyframes spin": {
      "0%": {
        transform: "rotate(360deg)"
      },
      "100%": {
        transform: "rotate(0deg)"
      }
    }
  })
)().rotateIcon;

export default useRotate;