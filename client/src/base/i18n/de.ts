export default {
    confirm: {
        okLabel: 'ok',
        cancelLabel: 'abbrechen'
    },
    table: {
        sortAscending: 'sortiere aufsteigend nach Spalte {{column}}',
        sortDescending: 'sortiere absteigend nach Spalte {{column}}',
        navigate: 'Seite {{page}} öffnen, Sätze {{from}} bis {{to}}',
        navigateFirst: 'erste Seite öffnen, Sätze {{from}} bis {{to}}',
        navigatePrevious: 'vorige Seite öffnen, Sätze {{from}} bis {{to}}',
        navigateNext: 'nächste Seite öffnen, Sätze {{from}} bis {{to}}',
        navigateLast: 'letzte Seite {{page}} öffnen, Sätze {{from}} bis {{to}}',
        navigateLeftUnshownPages: 'Seiten 1 - {{last}}',
        navigateRightUnshownPages: 'Seiten {{first}} - {{last}}',
    }
}