export default {
    confirm: {
        okLabel: 'ok',
        cancelLabel: 'cancel'
    },
    table: {
        sortAscending: 'sort ascending by column {{column}}',
        sortDescending: 'sort descending by column {{column}}',
        navigate: 'open page {{page}}, records {{from}} to {{to}}',
        navigateFirst: 'open first page, records {{from}} to {{to}}',
        navigatePrevious: 'open previous page, records {{from}} to {{to}}',
        navigateNext: 'open next page, records {{from}} to {{to}}',
        navigateLast: 'open last page {{page}}, records {{from}} to {{to}}',
        navigateLeftUnshownPages: 'pages from 1 to {{last}}',
        navigateRightUnshownPages: 'pages from {{first}} to {{last}}',
    }
}