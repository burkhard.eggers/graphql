import { i18n } from "i18next";
import de from "./de";
import en from "./en";

export default (i18n: i18n) => {
    i18n.addResourceBundle('de', 'components', de);
    i18n.addResourceBundle('en', 'components', en);
}