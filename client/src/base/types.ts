import { ReactNode, FunctionComponent } from "react"
import { DataLoader, EmeddableComponentProps } from "./components/Table/types"

export type ColumnRenderer<ROW,V> = FunctionComponent<{row: ROW, field: V}>

export type Route = {
    path: string,
    content: { [key: string]: ReactNode }
}

export type {DataLoader, EmeddableComponentProps}