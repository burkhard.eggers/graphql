import Context from "../../Context";
import { FunctionComponent, useContext, useEffect } from "react";
import styles from './selectedPerson.module.sass';
import Posts from "../../components/Posts";
import Loading from "../../components/Loading";
import { WithTranslation, useTranslation, withTranslation } from "react-i18next";
import { EmbeddedI18n, useEscapeStack } from "../../base";
import FormComponent from "../../components/Form";
import { useGetFullPerson, useUpdatePerson } from "../../hooks/api";
import { useParams } from "react-router-dom";
import Breadcrumbs from "../../components/Breadcrumbs";
import { Button, TextField } from '@mui/material';
import { FullPerson } from "@/../types";
import { Link } from "../../base";
import DetailDialog from './DetailDialog'

const SelectedPerson:FunctionComponent<WithTranslation> = ({ t }) => {
    const { 
        setLoadPerson, 
        setSelectedPerson, 
        selectedPerson, 
        setNameOfSelected, 
        setAgeOfSelected, 
        submitPerson, 
        loadPerson,
        setSelectedDetailsOpen 
    } = useContext(Context.context);
    const { personId } = useParams();
    const getFullPerson = useGetFullPerson();
    const updatePerson = useUpdatePerson();
    const { pushToStack, removeFromStack } = useEscapeStack('person-details');
    if(!personId) return <span>...</span>
    const load = () => {
        (async () => {
            setLoadPerson(true);
            await new Promise(r => setTimeout(r));
            const person = await getFullPerson({ id: parseInt(personId, 10) });
            await new Promise(r => setTimeout(r));
            setLoadPerson(false);
            setSelectedPerson(person);
        })()
    }
    useEffect(load, [])
    
    const setSelectedName = (evt: any) => {
        const { value } = evt.target;
        setNameOfSelected(value);
    }

    const setSelectedAge = (evt: any) => {
        const { value } = evt.target;
        setAgeOfSelected(value);
    }

    const submit = async () => {
        const selected = selectedPerson as FullPerson; 
        await updatePerson({ 
            personId: selected.id, 
            name: selected.name, 
            age: selected.age
        })
        submitPerson();

    }

    const closeDetails = () => {
        setSelectedDetailsOpen(false)
        removeFromStack()
    }

    const openDetails = () => {
        setSelectedDetailsOpen(true)
        pushToStack(closeDetails)
    }
    
    return <div 
            className={styles.container}
        >
        {loadPerson
            ? <Loading fill/>
            : (
                <>
                    <DetailDialog closeDialog={closeDetails}/>
                    <div className={styles.body}>
                        <Link to={`${location.pathname}/more`}>more...</Link>
                        <Button onClick={openDetails}>Details</Button>
                        <FormComponent>
                            <div>
                                <TextField
                                    className="text-input"
                                    label={t('selectedPerson.nameLabel')}
                                    value={selectedPerson?.name} onChange={setSelectedName}
                                    variant="standard"
                                />
                                <TextField
                                    className="text-input"
                                    label={t('selectedPerson.ageLabel')}
                                    value={selectedPerson?.age} onChange={setSelectedAge}
                                    type="number"
                                    variant="standard"
                                />
                                <EmbeddedI18n 
                                    i18nKey="selectedPerson.submit"
                                    inserts={{
                                        submit: ({ text }) => <Button 
                                                variant="contained"
                                                size="small"
                                                onClick={submit}
                                            >{text}</Button>
                                    }}
                                />
                            </div>
                        </FormComponent>
                        <div className={styles.posts}>
                            <Posts />
                        </div>
                    </div>
                </>    
            )
        }
        </div>
}

export const Header:FunctionComponent = () => {
    const {t} = useTranslation();
    const { selectedPerson } = useContext(Context.context);
    return (
        <>
            {t('selectedPerson.title', { name: selectedPerson?.name})}
        </>
    )
}

export const HeaderMore:FunctionComponent = () => {
    const {t} = useTranslation();
    return (
        <>
            <Breadcrumbs entries={[{ label: t('main.title'), path: '/'}]}/>
        </>
    )
}

export default withTranslation()(SelectedPerson);