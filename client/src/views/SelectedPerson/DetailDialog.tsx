import { Button, Dialog, DialogActions, DialogContent, DialogTitle } from "@mui/material";
import Context from "../../Context";
import { FunctionComponent, useContext } from "react";
import { useConfirm } from "../../base";

type Props = {
    closeDialog: () => void
}

const SelectedPersonDetailsDialog:FunctionComponent<Props> = ({ closeDialog }) => {
    const { selectedDetailsOpen, selectedPerson, setNameOfSelected } = useContext(Context.context);
    const confirm = useConfirm();
    const changeName = async () => {
        const ok = await confirm({
            msg: 'wirklich Namen ändern?',
            okLabel: 'weiter',
            title: 'Namen ändern'
        })
        if(ok){
            setNameOfSelected('Pete')
            await new Promise(r => setTimeout(r))
            closeDialog()
        }
    }
    return (
        <Dialog
            disableEscapeKeyDown
            open={!!selectedDetailsOpen}
            onClose={closeDialog}
        >
            <DialogTitle>Details</DialogTitle>
            <DialogContent>
                <b>Name</b>: {selectedPerson?.name}
                <br />
                <br />
                Namen auf <em>Pete</em> setzen
            </DialogContent>
            <DialogActions>
            <Button 
                color="primary" 
                variant="contained"
                onClick={changeName}
            >
                namen Ändern
            </Button>
            <Button 
                color="secondary" 
                variant="contained"
                onClick={closeDialog}
            >
                schliessen
            </Button>
            </DialogActions>
        </Dialog>
    )
}

export default SelectedPersonDetailsDialog;