import React, { FunctionComponent, useContext, useState } from "react";
import { withTranslation, WithTranslation} from 'react-i18next';
import { Button, TextField } from '@mui/material'
import Context from "../../Context";
import { useCreatePerson } from "../../hooks/api";
import {EmbeddedI18n} from "../../base";
import FormComponent from "../../components/Form";

const NewPerson: FunctionComponent<WithTranslation> = ({ t }) => {
    const { insertPerson } = useContext(Context.context);
    const [name, setName] = useState<string>('');
    const [age, setAge] = useState<number>(1);
    const create = useCreatePerson()
    const insert = async () => {
        const { id } = await create({ name, age })
        insertPerson({id, name, age});
    }
    return (
        <>
            <FormComponent title={t('newPerson.title') as string}>
                <TextField
                    className="text-input"
                    label={t('newPerson.nameLabel')}
                    onChange={(evt) =>{setName(evt.target.value)}}
                    value={name}
                    variant="standard"
                />
                <TextField
                    className="text-input"
                    label=  {t('newPerson.ageLabel')}
                    onChange={(evt) =>{setAge(parseInt(evt.target.value, 10))}}
                    value={age}
                    type="number"
                    variant="standard"
                />
                <EmbeddedI18n i18nKey="newPerson.submit" 
                    inserts={{
                        submit: ({text}) => <Button
                                variant="contained"
                                size="small" 
                                onClick={insert}
                            >
                                {text}
                            </Button>,
                        google: <a href="http://www.google.de">google</a>,
                        text: <b>ok?</b>
                    }}
                />
            </FormComponent>
        </>
    )
}
export default withTranslation()(NewPerson);