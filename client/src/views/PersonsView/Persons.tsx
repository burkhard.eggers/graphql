import { FunctionComponent, useContext } from "react";
import { withTranslation, WithTranslation } from 'react-i18next';
import DeletePersonLink from "./DeletePersonLink";
import Context from "../../Context";
import { FullPerson, Post } from "@/../types";
import { Column, FlowTable, useNavigate, subTable, Subtable } from "../../base";
import { useGetFullPerson } from '../../hooks/api'

const Persons: FunctionComponent<WithTranslation> = ({ t }) => {
    const { persons } = useContext(Context.context);
    const navigate = useNavigate();
    const getFullPerson = useGetFullPerson();
    const columns = [
        {
            id: 'id',
            headerLabel: t('main.idHeader'),
            sortable: true,
            scale: {
                width: 80
            }
        },
        {
            id: 'name',
            headerLabel: t('main.nameHeader'),
            sortable: true
        },
        {
            id: 'age',
            headerLabel: t('main.ageHeader'),
            sortable: true,
            scale: {
                width: 100
            }
        },
        {
            id: 'posts',
            headerLabel: t('main.nrPostsHeader'),
            sortable: true,
            value: (row) => row.posts.length,
            scale: {
                width: 100,
                hideWhenTableWidthLessThan: 600
            }
        },
        {
            id: 'actions',
            headerLabel: '',
            render: ({row}) => <DeletePersonLink {...row}/>,
            scale: {
                width: 30
            }
        }
    ] as Column<FullPerson, any>[]
    const rowExpansion = subTable<FullPerson, Post>(
        {
            rowKey: (row:Post) => `${row.id}`,
            expandable: (person: FullPerson) => person.posts.length > 0,
            columns: [{
                    id: 'id',
                    scale: {
                        width: 80,
                        hideWhenTableWidthLessThan: 600
                    },
                    headerLabel: 'id',
                    sortable: true
                },
                {
                    id: 'title',
                    scale: {
                        width: 200,
                        takeRemainingForTableWidth:{}
                    },
                    headerLabel: 'title',
                    sortable: true
                },
                {
                    id: 'content',
                    scale: {
                        width: 300,
                        hideWhenTableWidthLessThan: 600
                    },
                    headerLabel: 'content',
                    sortable: true
                }
            ],
            title: (ev) => <>Posts von <b>{ev.name}</b></>,
            empty: () => 'keine',
            markEvenOdd: true,
            pageSize: 5,
            getData: (person) => new Promise<Post[]>(async res => {
                const fp = await getFullPerson({id: person.id})
                res(fp.posts)
            })
        }
    )
    return (
        <FlowTable<FullPerson>
            rowKey={(row) => `${row.id}`}
            columns={columns}
            pageSize={5}
            rowClicked={(id) => navigate(`/person/${id}`)}
            data={persons}
            markEvenOdd
            rowExpansion={rowExpansion}
        />
    )
}

export default withTranslation()(Persons);