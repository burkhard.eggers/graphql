import React, { FunctionComponent, MouseEvent, useContext, useTransition } from "react";
import Context from "../../../src/Context";
import { useDeletePerson } from "../../hooks/api";
import { useTranslation } from 'react-i18next';
import { Person } from "@/../types";
import { DeleteOutline } from '@mui/icons-material';
import { Tooltip, IconButton } from '@mui/material';
import { useConfirm } from "../../base";

const DeletePersonLink: FunctionComponent<Person> = ({ id, name }) => {
    const { deletePerson} = useContext(Context.context);
    const deleteMutation = useDeletePerson();
    const {t} = useTranslation();
    const confirm = useConfirm()
    const del = (evt: MouseEvent) => {
        (async () => {
            evt.stopPropagation();
            const ok = await confirm({
                msg: t('main.deleteConfirm', { name })
            })
            if(!ok) return
            await deleteMutation({ id })
            deletePerson(id);
        })()
    }
    return <a onClick={del}>
        <Tooltip title={t('main.deleteTooltip')}>
            <IconButton sx={{padding: '0px'}}>
                <DeleteOutline />
            </IconButton>
        </Tooltip>
    </a> 
} 

export default DeletePersonLink;