import React, { FunctionComponent, useContext, useEffect} from 'react'
import Context from '../../Context';
import styles from './persons.module.sass';
import { useTranslation, withTranslation, WithTranslation } from 'react-i18next';
import NewPerson from './NewPerson';
import Loading from '../../components/Loading';
import Persons from './Persons';
import { useLoadPersons } from '../../hooks/api';

const App:FunctionComponent<WithTranslation> = ({ t }) => {
    const load = useLoadPersons();
    const {setPersons, persons, setLoadPerson} = useContext(Context.context);
    useEffect(() => {
        (async () => {
            setPersons([])
            await new Promise(r => setTimeout(r, 100))
            const pers = await load()
            setPersons(pers)
        })()
    }, []);
    return (
        <div className={styles.container}>
            <div className={styles.body}>
                <div className={styles.persons}>
                    { persons && persons.length > 0 
                    ? 
                    (
                        <>
                            <Persons />
                            <div className={styles.new}>    
                                <NewPerson />
                            </div> 
                        </>
                    )
                    :
                    <Loading big />
                    }
                </div>
            </div>
        </div>
    )
}

export const Header:FunctionComponent = () => {
    const {t} =useTranslation();
    return <>{t('main.title')}</>
} 
export default withTranslation()(App);