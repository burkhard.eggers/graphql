import { Column } from "@/src/base";
import { Event } from "@/../types";
import moment from "moment";

export default (t: any) => [
    {
        id: 'id',
        headerLabel: t('events.idLabel'),
        sortable: true,
        scale:{
            hideWhenTableWidthLessThan: 400,
            width: 80
        }
    },
    {
        id: 'title',
        headerLabel: t('events.titleLabel'),
        sortable: true,
        scale:{
            width: 200,
            takeRemainingForTableWidth: {
                to: 619
            }
        }
    },
    {
        id: 'description',
        sortable: true,
        headerLabel: t('events.descriptionLabel'),
        scale:{
            width: 200,
            hideWhenTableWidthLessThan: 620,
            takeRemainingForTableWidth: {
                from: 620
            }
        }
    },
    {
        id: 'date',
        headerLabel: t('events.dateLabel'),
        scale:{
            width: 220,
            hideWhenTableWidthLessThan: 550
        },
        sortable: (value) => value.toString(),
        value: (row) => row.date,
        render: ({field}: any) => <>{moment(new Date(field)).format("hh:mm:ss DD:MM:YYYY")}</>
    } as Column<Event, number>
] as Column<Event, any>[]
