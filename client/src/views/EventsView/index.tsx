import { FlowTable, ScalingTable } from "../../base";
import { FunctionComponent, useEffect } from "react";
import { WithTranslation, useTranslation, withTranslation } from "react-i18next";
import { Event } from "@/../types";
import columns from './columns';
import { useEventsForTable } from "../../hooks/api";
import { PagedTable, TablePaging } from "@/src/base";
import { EmeddableComponentProps } from "@/src/base/types";

const Embedded: FunctionComponent<EmeddableComponentProps<Event>> = 
    ({ row, onHeightChange, onInitialHeightKnown }) => {
        useEffect(onInitialHeightKnown, [])
    return (
        <div>
            <h1>Title</h1>
            {row.title}
        </div>
    )
} 

const EventsView:FunctionComponent<WithTranslation> = ({t}) => {
    const eventsForTable = useEventsForTable();
    const load = (paging: TablePaging) => new Promise<PagedTable<Event>>( async (res) => {
        const resp = await eventsForTable({
                paging
            });
        res(resp)
        })
        const rowClicked = (id: string) => alert('row clicked ' + id)
        const rowExpansion = {
            component: (event:Event) => 
                new Promise<FunctionComponent<EmeddableComponentProps<Event>>>(async res => {
                await new Promise(r => setTimeout(r, 1000))
                res(Embedded)                
            }) 
        } 
        const renderFlow = false
        return (
        <>
        {renderFlow ?
            <FlowTable<Event>
                data={load}
                columns={columns(t)}
                rowKey={(evt) => `${evt.id}`}
                pageSize={15}
                markEvenOdd
                rowClicked={rowClicked}
                rowExpansion={rowExpansion}
            />
            :
            <ScalingTable<Event>
                data={load}
                columns={columns(t)}
                rowKey={(evt) => `${evt.id}`}
                rowHeight={18}
                markEvenOdd
                rowClicked={rowClicked}
                rowExpansion={rowExpansion}
            />}
        </>
    )
}

export const Header:FunctionComponent = () => {
    const {t} = useTranslation();
    return <>{t('events.title')}</>
} 
export default withTranslation()(EventsView);