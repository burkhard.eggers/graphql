import React from 'react';
import PersonsView, {Header as PVHeader} from './views/PersonsView';
import EventsView, { Header as EvHeader} from './views/EventsView';
import { Route } from '@/src/base';
import SelectedPerson, { HeaderMore, Header as SPHeader } from './views/SelectedPerson';
import { RoutedDialog } from './base';

export default [
  {
    path: '/person/:personId/more',
    content: {
      main: <SelectedPerson />,
      header: <SPHeader />,
      headerMore: <HeaderMore />,
      ontop: (
        <RoutedDialog
          onOk={() => new Promise(r => setTimeout(r, 1000))}
          onCancel={() => new Promise(r => setTimeout(r, 1000))}
          title={<span>more <b>title</b></span>}
          backUrl={(url) => url.split('/').slice(0, -1).join('/')}
        >
          more
        </RoutedDialog>
        )
    }
  },
  {
    path: '/person/:personId',
    content: {
      main: <SelectedPerson />,
      header: <SPHeader />,
      headerMore: <HeaderMore />
    }
  },
  {
    path: '/events',
    content: {
      main: <EventsView />,
      header: <EvHeader />
    }
  },
  {
    path: "/",
    content: {
      main: <PersonsView />,
      header: <PVHeader />
    }
  }
] as Route[];