import { ThemeOptions } from '@mui/material/styles'

export default {
    palette: {
        background: {
            light: '#f8f8f8',
            main: '#eee',
            dark: '#ccc'
        },
        secondary: {
            light: '#efea4f',
            main: '#fddb33',
            dark: '#996b07'
        },
        primary: {
            light: '#ddf',
            main: '#55f',
            dark: '#009'
        },
    },
  } as ThemeOptions;