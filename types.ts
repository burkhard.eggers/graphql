export interface Person {
    id: number,
    name: string,
    age: number
  }
  
  export type FullPerson = Person & {
    posts: Post[]
  }
  
  export interface Post {
    id: number,
    title: string,
    content: string,
    personId: number
  }

  export interface Event {
    id: number,
    title: string,
    description: string,
    date: number
  }

  export type Paged<T> = {
    entries: T[],
    totalEntries: number
  }

  export interface PagingInput {
    sortColumn?: string,
    sortAscending?: boolean,
    from: number,
    to: number
  }