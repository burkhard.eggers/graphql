import { ApolloServer } from 'apollo-server-express';
import { PubSub, withFilter } from 'graphql-subscriptions';
import express from 'express';
import { readFileSync } from 'fs';
import {resolve, dirname} from 'path';
import { 
  getPersons, 
  pushPerson, 
  setPersons, 
  generateId, 
  fullPersons, 
  fullPosts, 
  pushPost, 
  allEvents,
  events
} from './fakeDB.js';
import { Person } from '../../types.js';
import { makeExecutableSchema } from '@graphql-tools/schema';
import { createServer } from 'http';
import { ApolloServerPluginDrainHttpServer } from '@apollo/server/plugin/drainHttpServer';
import { WebSocketServer } from 'ws';
import { useServer } from 'graphql-ws/lib/use/ws';


const __dirname = resolve(dirname(''));
const schema = readFileSync(resolve(__dirname, 'src/schema.graphql')).toString();

const typeDefs = `#graphql
${schema}
`;

const pubsub = new PubSub();

console.log('schema: ' + typeDefs);

const resolvers = {
    Query: {
      persons: () => fullPersons(),
      posts: () => fullPosts(),
      post: (obj, { id }) => fullPosts().find(post => post.id === id),
      person: (obj, { id }) => fullPersons().find(person => person.id === id),
      allEvents: () => allEvents(),
      events: (obj, {paging}) => events(paging)
    },
    Mutation: {
      updatePerson: (obj, { id, person: { name, age } }) => {
        setPersons(getPersons().map(person => person.id === id ? { ...person, name, age } : person))
        return fullPersons().find(person => person.id === id)
      },
      insertPerson: (obj, {person: { name, age }}) => {
        const id = generateId();
        const person: Person = { name, age, id }; 
        pushPerson(person);
        pubsub.publish('PERSON_CREATED', {personCreated: person});

        return fullPersons().find(person => person.id === id)
      },
      deletePerson: (obj: any, { id }: any) => {
        console.log('delete person', id);
        const persons = getPersons().filter(p => id !== p.id);
        setPersons(persons);
      },
      doPost: (obj, { personId, content, title}) => {
        const id = generateId();
        pushPost({ content, title, id, personId });
        return fullPosts().find(post => post.id === id);
      }
    },
    Subscription: {
      personCreated: {
        subscribe: () => pubsub.asyncIterator('PERSON_CREATED')
      },
    }
  };
(async () => {
  const schema = makeExecutableSchema({ typeDefs, resolvers });
  const app = express();
  
  const httpServer = createServer(app);

  // Creating the WebSocket server
  const wsServer = new WebSocketServer({
    server: httpServer,
    path: '/suscriptions',
  });
  
  const server = new ApolloServer({
    schema,
    plugins: [
      // Proper shutdown for the HTTP server.
      ApolloServerPluginDrainHttpServer({ httpServer }) as any,
  
      // Proper shutdown for the WebSocket server.
      {
        async serverWillStart() {
          return {
            async drainServer() {
              await useServer({ schema }, wsServer).dispose();
            },
          };
        },
      },
    ]
  });

  await server.start();
  server.applyMiddleware({ app, path: '/graphql'})
  
  httpServer.listen(4000, () => {
    const {port} = httpServer.address() as any;
    console.log(`🚀  Server ready at http://localhost:${port}`);
  })
})()
