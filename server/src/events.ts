import { Event, PagingInput } from '../../types';

const events = [] as Event[];

const vowels = 'aeiouy'
const consonants = 'bcdfghjklmnpqrstvwx' 
const randomWord = () => {
    const length = (Math.random() * 5) + 3
    let ret = ''
    let useV = Math.random() > 0.5
    for (let index = 0; index < length; index++) {
       useV = !useV
       const base = useV ? vowels : consonants
       const which = Math.floor(Math.random() * base.length)
       const char = base.substring(which, which + 1)
       ret = `${ret}${char}` 
    }
    return ret
} 

const randomString = (minWords: number, maxWords: number) => {
    let ret
    const words = Math.floor(Math.random() * (maxWords - minWords) + minWords)
    for (let index = 0; index < words; index++) {
        ret = ret ? `${ret} ${randomWord()}` : randomWord();
    }
    return ret;
}

const randomDate = () => {
    return Date.now() + Math.floor(Math.random() * 1000 * 3600 * 24 * 365)
}

for (let index = 0; index < 1000; index++) {
    const event = {
        id: 10000 + index,
        title: randomString(1, 3),
        description: randomString(2, 8),
        date: randomDate(),
    } as Event
    events.push(event)
}

export const paged = (paging: PagingInput) => {
    console.log('paging', paging)
    const sorter = (e1, e2): number => {
        const e1v = e1[paging.sortColumn || 'id']
        const e2v = e2[paging.sortColumn || 'id']
        let compare = 0;
        if(e1v !== e2v) compare = e1v > e2v ? 1 : -1;
        if(!paging.sortAscending) compare *= -1;
        return compare
    }
    const evCl = [...events.map(e => ({...e}))]
    evCl.sort(sorter)
    const entries = evCl.slice(paging.from, paging.to)
    return {
        totalEntries: events.length,
        entries
    }
}

export default () => events;