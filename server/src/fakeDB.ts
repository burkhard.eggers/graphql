import { PagingInput, Person, Post} from '../../types'
import fakeEvents, { paged } from './events.js';

let idCount = 1;
export const generateId = (): number => idCount++;

let persons: Person[] = [
    {
      id: generateId(),
      name: 'Burkhard',
      age: 55
    },
    {
      id: generateId(),
      name: 'Lucia',
      age: 58
    },
    {
      id: generateId(),
      name: 'Caro',
      age: 18
    },
    {
      id: generateId(),
      name: 'Kaina',
      age: 23
    },
    {
      id: generateId(),
      name: 'Johan David',
      age: 35
    },
  ];

  let posts: Post[] = [
    {
      id: generateId(),
      title: 'post 1',
      content: 'ohoihoi oh oiho iho iho iho ',
      personId: persons[0].id
    },
    {
      id: generateId(),
      title: 'post 2',
      content: 'ohoihoi oh oiho iho iho iho ',
      personId: persons[0].id
    }
  ]

export const setPersons = (pers: Person[]) => persons = pers; 
export const getPersons = (): Person[] => persons; 

export const pushPerson = (person: Person) => persons.push(person);
export const pushPost = (post: Post) => posts.push(post);

export const fullPosts = () => posts.map(post => ({...post, person: persons.find(person => person.id === post.personId )}));
export const fullPersons = () => persons.map(person => ({...person, posts: posts.filter(p => p.personId === person.id)}));

export const allEvents = () => fakeEvents()

export const events = (paging: PagingInput) => paged(paging)